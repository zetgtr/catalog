<?php

namespace Catalog\Remove;

use App\Models\Admin\Menu;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class RemovePackage
{
    private $pathMigration;
    private $pathScript;
    private $pathVues;
    private $pathPages;
    private $pathComponents;
    public function __construct()
    {
        $this->pathMigration = database_path('migrations');
        $this->pathScript = resource_path('js/catalog');
        $this->pathVues = resource_path('views/vendor/catalog');
        $this->pathPages = resource_path('js/Pages/Catalog');
        $this->pathComponents = resource_path('js/Components/catalog');
    }

    public function run($settings,$migration = false, $script = false, $vies = false)
    {
        chdir(base_path());
        if($migration)
        {
            $filterCategoryName = "2023_09_15_140229_create_catalog_filter_category_table.php";
            $filterCategoryPath = $this->pathMigration."/".$filterCategoryName;

            $catalogFilterName = "2023_09_15_140230_create_catalog_filters_table.php";
            $catalogFilterPath = $this->pathMigration."/".$catalogFilterName;

            $productFilterName = "2023_09_15_140733_create_catalog_filter_has_catalog_product_table.php";
            $productFilterPath = $this->pathMigration."/".$productFilterName;

            $productsName = "2023_05_03_113423_create_catalog_products_table.php";
            $productsPath = $this->pathMigration."/".$productsName;

            $cart = "2023_08_03_145445_create_cart_table.php";
            $cartPath = $this->pathMigration."/".$cart;

            $orderHasProduct = "2023_08_04_111323_create_order_has_product_table.php";
            $orderHasProductPath = $this->pathMigration."/".$orderHasProduct;

            $orderName = "2023_05_03_113435_create_catalog_orders_table.php";
            $orderPath = $this->pathMigration."/".$orderName;

            $categoriesName = "2023_05_03_113528_create_catalog_categories_table.php";
            $categoriesPath = $this->pathMigration."/".$categoriesName;

            $categoryHasProductName = "2023_05_03_113551_create_catalog_category_has_product_table.php";
            $categoryHasProductPath = $this->pathMigration."/".$categoryHasProductName;

            $settingsName = "2023_05_03_113602_create_catalog_settings_table.php";
            $settingsPath = $this->pathMigration."/".$settingsName;

            if (File::exists($productFilterPath)) {
                Artisan::call('migrate:rollback --path=database/migrations/'.$productFilterName);
                unlink($productFilterPath);
            }
            if (File::exists($catalogFilterPath)) {
                Artisan::call('migrate:rollback --path=database/migrations/'.$catalogFilterName);
                unlink($catalogFilterPath);
            }
            if (File::exists($filterCategoryPath)) {
                Artisan::call('migrate:rollback --path=database/migrations/'.$filterCategoryName);
                unlink($filterCategoryPath);
            }
            
            
            if (File::exists($orderHasProductPath)) {
                Artisan::call('migrate:rollback --path=database/migrations/'.$orderHasProduct);
                unlink($orderHasProductPath);
            }

            if (File::exists($orderHasProductPath)) {
                Artisan::call('migrate:rollback --path=database/migrations/'.$orderHasProduct);
                unlink($orderHasProductPath);
            }

            if (File::exists($cartPath)) {
                Artisan::call('migrate:rollback --path=database/migrations/'.$cart);
                unlink($cartPath);
            }

            if (File::exists($orderPath)) {
                Artisan::call('migrate:rollback --path=database/migrations/'.$orderName);
                unlink($orderPath);
            }
            if (File::exists($categoryHasProductPath)) {
                Artisan::call('migrate:rollback --path=database/migrations/'.$categoryHasProductName);
                unlink($categoryHasProductPath);
            }
            if (File::exists($categoriesPath)) {
                Artisan::call('migrate:rollback --path=database/migrations/'.$categoriesName);
                unlink($categoriesPath);

            }
            if (File::exists($productsPath)) {
                Artisan::call('migrate:rollback --path=database/migrations/'.$productsName);
                unlink($productsPath);
            }

            if (File::exists($settingsPath)) {
                Artisan::call('migrate:rollback --path=database/migrations/'.$settingsName);
                unlink($settingsPath);
            }


            $menu = Menu::query()->find(1001);
            if($menu)
                $menu->delete();
        }

        if($script)
        {
            if (File::isDirectory($this->pathScript))
                File::deleteDirectory($this->pathScript);
        }

        if ($vies)
        {
            if (File::isDirectory($this->pathVues))
                File::deleteDirectory($this->pathVues);
            if (File::isDirectory($this->pathPages))
                File::deleteDirectory($this->pathPages);
            if (File::isDirectory($this->pathComponents))
                File::deleteDirectory($this->pathComponents);
        }
    }
}
