<form id="form_promo" action="{{ old('id') > 0 ? route('admin.catalog.promocode.update',old('id')) : route('admin.catalog.promocode.store') }}" method="POST" class="row">
    @csrf
    @if(old('id') > 0)
        <input type="hidden" name="_method" id="route_put" value="put">
    @endif
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="col-12">
        <div class="form-group">
            <label>Промокод</label>
            <input type="text" name="promocode" value="{{ old('promocode') }}" class="form-control @error('promocode') is-invalid @enderror">
            <x-error error-value="promocode" />
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Скидка</label>
            <input type="number" name="sale" value="{{ old('sale') }}" class="form-control @error('sale') is-invalid @enderror">
            <x-error error-value="sale" />
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Тип скидки</label>
            <select type="number" name="type" class="form-control form-select @error('type') is-invalid @enderror">
                @foreach(\Catalog\Enums\PromocodeEnum::all() as $type)
                    <option @selected(old('type') == $type) value="{{ $type }}">{{ \Catalog\Enums\PromocodeEnum::name($type) }}</option>
                @endforeach
            </select>
            <x-error error-value="type" />
        </div>
    </div>
    <div class="col-12">
        <button class="btn btn-success info_success btn-sm mb-3 me-2">@if(old('id') > 0) Обновить @else Добавить @endif</button>
        <button class="btn btn-danger @if(!old('id')) d-none @endif info_danger btn-sm mb-3 me-2">Отменить</button>
    </div>
</form>
<input type="hidden" id="route_store" value="{{ route('admin.catalog.promocode.store')  }}">
<input type="hidden" name="_method" id="route_put" value="put">
@vite('resources/js/catalog/promocode/promocode.js')
