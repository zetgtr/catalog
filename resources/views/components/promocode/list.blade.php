<table id="example4" class="table table-bordered text-nowrap border-bottom">
    <thead>
    <tr>
        <th class="border-bottom-0 text-center">Промокод</th>
        <th class="border-bottom-0 text-center">Скидка</th>
        <th class="border-bottom-0 text-center">Тип скидки</th>
        <th class="border-bottom-0 text-center">Действие</th>
    </tr>
    </thead>
    <tbody>
    @foreach($promocodes as $promocode)
        <tr class="delete-element">
            <td class="text-center">{{ $promocode->promocode }}</td>
            <td class="text-center">{{ $promocode->sale }}</td>
            <td class="text-center">{{ \Catalog\Enums\PromocodeEnum::name($promocode->type) }}</td>
            <td class="text-center">
                <a href="{{ route('admin.catalog.promocode.edit',$promocode) }}" class="btn btn-success edit-promocode btn-sm pull-right"
                   data-action="{{ route('admin.catalog.promocode.update',$promocode) }}" data-id="{{ $promocode->id }}">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
                <a href="{{ route('admin.catalog.promocode.destroy',$promocode) }}" class="delete btn btn-danger btn-sm pull-right">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<script src="{{ asset('assets/js/table-data.js')}}"></script>
<script src="{{ asset('assets/js/admin/delete.js')}}"></script>
@vite('resources/js/catalog/promocode/promocode.js')
