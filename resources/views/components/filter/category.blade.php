<div class="row category-filter-container">
    <div class="col-lg-8">
        <div class="dd nestable" id="nestable">
            <x-catalog::filter.dd :categories="$categories" type="categories" route='filter_category' />
        </div>
        <br>
    </div>
    <form action="{{ route('admin.catalog.filter_category.store') }}" id="filter_category" method="post" class="col-lg-4">
        @csrf
        <input type="hidden" name="id" id="id_category">
        <h4 class="info_title">Добавить категорию</h4>
        <div class="form-group">
            <label for="alias" class="form-label">Alias</label>
            <input type="text" id="alias" name="alias" value="" class="form-control">
        </div>
        <div class="form-group">
            <label for="name" class="form-label">Название</label>
            <input type="text" id="name_category" name="name" value="" class="form-control">
        </div>
        <div class="form-group">
            <label for="name" class="form-label">Тип</label>
            <select type="text" id="type_filter" name="type" class="form-select">
                <option selected="" disabled="" value="">--Выберите--</option>
                <option value="checkbox">Множественный выбор</option>
                <option value="slider-range">Бегунок</option>
                <option value="text">Текст</option>
                <option value="property">Свойства</option>
            </select>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <label for="open" class="form-label">Скрыть на сайте</label>
                    <label class="custom-switch ps-0">
                        <input type="checkbox" name="hidden" value="1" id="hidden" class="custom-switch-input">
                        <span class="custom-switch-indicator me-3"></span>
                    </label>
                </div>
                <div class="col-12 col-sm-6">
                    <input type="hidden" name="hidden_admin" value="0">
                    <label for="open" class="form-label">Показать сразу открытой</label>
                    <label class="custom-switch ps-0">
                        <input type="checkbox" name="open" id="open" class="custom-switch-input">
                        <span class="custom-switch-indicator me-3"></span>
                    </label>
                </div>
                <div class="col-12 col-sm-6">
                    <label for="open" class="form-label">Скрыть в админке</label>
                    <label class="custom-switch ps-0">
                        <input type="checkbox" name="hidden_admin" value="1" id="hidden_admin"
                            class="custom-switch-input">
                        <span class="custom-switch-indicator me-3"></span>
                    </label>
                </div>
            </div>
        </div>
        <button class="btn btn-success info_success btn-sm mb-3 me-2">Добавить</button>
        <button class="btn btn-danger d-none info_danger btn-sm mb-3 me-2">Отменить</button>
    </form>
</div>
<input type="hidden" id="route_dd" value="{{ route('admin.catalog.filter_category.order') }}">
<input type="hidden" id="route_store" value="{{ route('admin.catalog.filter_category.store') }}">
<input type="hidden" name="_method" id="route_put" value="put">

@vite('resources/js/catalog/filter/category.js')
<script>
    const countDnd = 1
</script>
<script src="{{ asset('assets/js/admin/delete.js') }}"></script>
<script src="{{ asset('assets/js/admin/dnd.js') }}"></script>
