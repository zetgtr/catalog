<div class="row filter-container">
    <div class="col-lg-8">
        <div class="dd nestable" id="nestable">
            <x-catalog::filter.dd :categories="$filters" type="filter" route='filter' />
        </div>
        <br>
    </div>
    <form action="{{ route('admin.catalog.filter.store') }}" id="info_form" method="post" class="col-lg-4">
        @csrf
        <input type="hidden" name="id" id="id_filter">
        <h4 class="info_title">Добавить фильтр</h4>
        <div class="form-group">
            <label for="alias" class="form-label">Alias</label>
            <input type="text" id="alias_filter" name="alias" value="" class="form-control">
        </div>
        <div class="form-group">
            <label for="name" class="form-label">Название</label>
            <input type="text" id="name_filter" name="name" value="" class="form-control">
        </div>
        <div class="form-group">
            <label for="category" class="form-label">Категория</label>

            <select type="text" id="category_filter" name="category_id"
                    class="form-select">
                <option selected disabled value="">--Выберите--</option>
                @foreach ($categories as $item)
                    <option @selected(old('category_id', $category->id) == $item->id) value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
            <x-error error-value="job" />
        </div>
        <button class="btn btn-success info_success btn-sm mb-3 me-2">Добавить</button>
        <button class="btn btn-danger d-none info_danger btn-sm mb-3 me-2">Отменить</button>

    </form>
    <input type="hidden" id="route_dd" value="{{ route('admin.catalog.filter.order') }}">
    <input type="hidden" id="route_store" value="{{ route('admin.catalog.filter.store') }}">
    <input type="hidden" name="_method" id="route_put" value="put">
</div>

@vite('resources/js/catalog/filter/filter.js')
<script>
    const countDnd = 1
</script>
<script src="{{ asset('assets/js/admin/delete.js') }}"></script>
<script src="{{ asset('assets/js/admin/dnd.js') }}"></script>
