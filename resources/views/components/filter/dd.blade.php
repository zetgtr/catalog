@if(count($categories) > 0)
    <ol class="dd-list">
        @foreach($categories as $item)
            <li class="dd-item delete-element" data-indent="{{ $type }}"  data-id="{{$item->id}}" data-name="{{$item->name}}">
                <input type="hidden" class="route_update" value="{{  route('admin.catalog.'.$route.'.update',[$route=>$item]) }}">
                <div class="dd-handle">
                    <span>{{$item->name}}</span>
                </div>
                <div class="dnd-edit">
                    @if($route == 'filter_category')
                        <a href="{{route('admin.catalog.filter.show', ['filter'=>$item])}}" class="button-show btn btn-warning btn-xs pull-right"
                           data-owner-id="1">
                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                        </a>
                    @endif
                    <a href="{{route('admin.catalog.'.$route.'.destroy', [$route=>$item])}}" class="button-delete delete btn btn-danger btn-xs pull-right"
                       data-owner-id="1">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a>
                    <a href="{{route('admin.catalog.'.$route.'.edit', [$route=>$item])}}" class="button-edit btn btn-success btn-xs pull-right"
                       data-owner-id="1">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                </div>
            </li>
        @endforeach
    </ol>
@endif
<style>
    .button-show{
        position: absolute;
        top: 4px;
        right: 52px;
    }
</style>
