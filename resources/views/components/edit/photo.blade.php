<div class="tab-pane fade" id="photo" aria-labelledby="photo-link">
    <div class="form-group ">
        <label for="">Изображения</label>
        <input type="file" class="filepond " name="img[]" multiple>
        <div class="input-group">
            <span class="input-group-btn">
              </span>
            <x-error error-value="img" />
        </div>
        <div class="pb-0 mt-3">
            <ul id="lightgallery" class="list-unstyled row">
                @if(old("images",$category->images != '[]' ? $category->images : []))
                    @foreach(explode(",",  implode(",", old("images", $category->images))) as $image)
                        <li class="col-xs-6 col-sm-4 col-md-4 col-xl-4 mb-5 border-bottom-0 delete-element"
                            data-responsive="{{$image}}"
                            data-src="{{$image}}">
                            <a href="{{ route('admin.catalog.category.delete_image',[$category,str_replace('/','@',$image)]) }}" style="position: relative; display: inline-block;" >
                                <img class="img-responsive br-5" style="height: 200px;" src="{{$image}}" alt="Thumb-1">
                                <i href="{{ route('admin.catalog.category.delete_image',[$category,str_replace('/','@',$image)]) }}" class="fas fa-times delete" style="position: absolute;top: 10px;right: 10px;font-size: 15px;"></i>
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/admin/delete.js') }}"></script>
<script src="{{ asset('assets/plugins/gallery/lightgallery.js') }}"></script>
<script src="{{ asset('assets/plugins/gallery/lightgallery-1.js') }}"></script>
