<section class="tab-pane fade product-property" id="property" aria-labelledby="property-link">
    <input type="hidden" name="property_count" value="{{ $product->property->count() }}">
    <div class="row">
        <div class="col-12 form-group">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-xl-6">
                    <label class="d-flex justify-content-between align-items-center">Свойства</label>
                    <div class="d-flex gap-1">
                        <select id="property_select" name="property_select" class="form-control form-select">
                            <option value="0" selected disabled>-- Выберите --</option>
                            @foreach ($product->property as $property)
                                <option @selected(old('property_select') === $property->id) value="{{ $property->id }}">{{ $property->title }}
                                </option>
                            @endforeach
                        </select>
                        <a class="btn btn-secondary btn-edit-property" href="#"><i
                                class="fal fa-pencil-alt flex-shrink-0 "></i></a>
                        <button formaction="{{ route('admin.catalog.property.save', $product) }}"
                                class="btn btn-success btn-save-property position-relative flex-shrink-0"
                                style="top: 0; right: 0">Сохранить характеристики</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 container_property">
            <div class="row">
                @foreach ($filters as $category)
                    <div class="{{ $category->type === 'checkbox' ? 'col-lg-4' : 'col-lg-3' }} form-group">
                        @if ($category->type == 'checkbox')
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse-{{ $category->alias }}" aria-expanded="false"
                                    aria-controls="collapse-{{ $category->alias }}">
                                <strong class="accordion-title">{{ $category->name }}</strong>
                            </button>
                        @else
                            <label>{{ $category->name }}</label>
                        @endif
                        @if ($category->type == 'checkbox')
                            <div id="collapse-{{ $category->alias }}"
                                 class="accordion-collapse collapse show custom-scroll"
                                 aria-labelledby="heading-{{ $category->alias }}">
                                @foreach ($category->filter->sortBy('name') as $filter)
                                    <div class="block-scroll-wrap custom-scroll">
                                        <div class="form-check gap-1 d-flex align-items-center ">
                                            <input class="form-check-input m-0 position-static"
                                                   type="{{ $category->type }}" @checked(old('filters.' . $filter->alias, $product->filters->contains($filter->id)))
                                                   value="1" name="filters[{{ $filter->alias }}]"
                                                   id="{{ $filter->alias }}">
                                            <label class="form-check-label" for="{{ $filter->alias }}">
                                                {{ $filter->name }}
                                            </label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            @foreach ($category->filter as $filter)
                                @php
                                    $valueFilter = $product
                                        ->filters()
                                        ->where('filter_id', $filter->id)
                                        ->first();
                                @endphp
                                <input type="text" name="filters[{{ $filter->alias }}]" id="{{ $filter->alias }}"
                                       class="form-control col-12 "
                                       value="{{ old($filter->alias, $valueFilter ? $valueFilter->pivot->value : '') }}">
                            @endforeach
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<input type="hidden" value="{{ $product->property }}" id="product_property">
<input type="hidden" value="{{ $product->id }}" id="product_id">
<input type="hidden" value="{{ $product->filters }}" id="filters_product">
<input type="hidden" value="{{ route('admin.catalog.property.order', $product) }}" id="route_dd">
<x-catalog::product.edit.template-property />
<x-admin.modal.modalWindow />
@vite('resources/js/catalog/property/index.js')
