<template id="table_property">
    <div class="dd nestable" id="nestable">
        <span>Свойства отсутствуют</span>
    </div>
</template>


<template id="add_property">
    <form>
        <div class="form-group">
            <label class="w-100 d-flex justify-content-start">Название</label>
            <input type="text" name="title" class="form-control">
        </div>
        <div class="form-group">
            <label class="w-100 d-flex justify-content-start">Alias свойства</label>
            <input type="text" name="filter_alias" class="form-control">
        </div>
    </form>
</template>
