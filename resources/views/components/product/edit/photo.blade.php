<div class="tab-pane fade" id="photo" aria-labelledby="photo-link">
    <div class="form-group ">
        <label for="">Изображения</label>
        <input type="file" class="filepond " name="img[]" multiple>
        <div class="input-group">
            <span class="input-group-btn">
              </span>
            <x-error error-value="img" />
        </div>
    </div>
</div>


<input type="hidden" value="{{ $product->images }}" id="images_filepond">

<script src="{{ asset('assets/plugins/gallery/lightgallery.js') }}"></script>
<script src="{{ asset('assets/plugins/gallery/lightgallery-1.js') }}"></script>
