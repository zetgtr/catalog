@foreach ($categories as $category)
<div class="category__item">
    <a href="{{ route('catalog.category',['category'=>$category->url]) }}" class="shadow-bottom-black">
        <img class="@if($category->images) have-img @endif" src="{{ $category->images ? $category->images[0] : "/assets/img/no-image.png" }}" alt="{{ $category->title }}">
        <span class="title">
            {{ $category->title }}
        </span>
    </a>
</div>
@endforeach
