<template id='templItem'>
    <div class="catalog__product__item">
        <a href="http://localhost/catalog/product/tovar-bez-kartinki" class="img-container">
            <img src="/assets/img/popular/popular01.jpg" alt="Товар без картинки" class="product__img ">
        </a>
        <a href="http://localhost/catalog/product/tovar-bez-kartinki" class="title-container">
            <h4 class="title">Товар без картинки</h4>
        </a>
        <div class="weight">
            <p>Толщина: <span>200</span> мк</p>
        </div>
        <div class="price">
            <p><span>12321</span>/пог. м</p>
        </div>
        <button class="button button-s popular__btn" data-product="" onclick="addToCartEventHandler.call(this)" data-id="5">
            <span>в корзину</span>
        </button>
    </div>
</template>
