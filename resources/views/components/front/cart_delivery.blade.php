<section class="cart--delivery order-2">
    <input type="hidden" id="selectedAddressInput">
    <input type="hidden" id="typeDelivery">

    <h2>Доставка</h2>
    <div class="cart--delivery-lsit">
        <ul class="nav cart--delivery-container" id="myTab" role="tablist">
            <li class="nav-item">
                <input id="delivery-1" type="radio" name="delivery" value="1" checked class="form-check-input">
                <label class="nav-link " id="home-tab" data-bs-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" for="person">Самовывоз</label>
            </li>
            <li class="nav-item">
                <input id="delivery-2" type="radio" name="delivery" value="2" class="form-check-input">
                <label class="nav-link active" id="profile-tab" data-bs-toggle="tab" href="#profile" role="tab" aria-controls="home" aria-selected="true" for="person">Доставка по Санкт-Петербургу</label>
            </li>
            <li class="nav-item">
                <input id="delivery-3" type="radio" name="delivery" value="3" class="form-check-input">
                <label class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#contact" role="tab" aria-controls="home" aria-selected="true" for="person">Доставка по России</label>
            </li>
        </ul>
    </div>
    <div class="cart--delivery-content" id="myTabContent">
        <div class="tab-pane " id="home" role="tabpanel" aria-labelledby="home-tab">
            <p class="text-delivery">Самовывоз осуществляется по адресу:<br> г. Санкт-Петербург, пр. Александровской фермы, д.29ВГГ</p>
            <div class="cart-order-wrapper order-3">
                <div class="cart__delivery-price">
                    <h3 class="cart__delivery-price-title">Сумма доставки:</h3>
                    <span class="dost-null">0</span>
                </div>
                <div class="cart__all-price">
                    <h3 class="cart__all-price-title">Сумма заказа:</h3>
                    <span class="cart__all-price-title-main">{{ $total }}</span>
                </div>
                <div class="cart__all-price">
                    <h3 class="cart__all-price-title">Итого:</h3>
                    <span class="cart__all-price-title-main">{{ $total }}</span>
                </div>
                <div class="order-foot__send"></div>
            </div>
        </div>
        <div class="tab-pane show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div class="cart--delivery__spb-container">
                <p class="text-delivery">Мы доставляем заказы по любому адресу в городе Санкт-Петербург.</p>
                <p class="text-delivery"><strong>Доставка по Санкт-Петербургу - 300 руб., от 5 000 руб. - бесплатно.</strong></p>
                <div class="cart-form__item required">
                    <div class="form-warning cart-form__warning">Введите адрес доставки (улица, дом, квартира)</div>
                    <label for="" class="cart-form__label">Введите адрес доставки (улица, дом, квартира)</label>
                    <input name="name" id="addressInput2" type="text" class="cart-form__input">
                    <ul id="suggestionsList2" class="cart-form__address-lines address-lines list-unstyled"></ul>
                </div>
                <div class="row">
                    <div class="cart-form__item required col-xl-4">
                        <div class="form-warning cart-form__warning">Введите подъезд</div>
                        <label for="" class="cart-form__label">Подъезд</label>
                        <input name="entrance" type="text" class="cart-form__input">
                    </div>
                    <div class="cart-form__item required col-xl-4">
                        <div class="form-warning cart-form__warning">Введите этаж</div>
                        <label for="" class="cart-form__label">Этаж</label>
                        <input name="entrance" type="text" class="cart-form__input">
                    </div>
                    <div class="cart-form__item required col-xl-4">
                        <div class="form-warning cart-form__warning">Введите домофон</div>
                        <label for="" class="cart-form__label">Домофон</label>
                        <input name="entrance" type="text" class="cart-form__input">
                    </div>
                </div>
            </div>

            <div class="cart-order-wrapper order-3">
                @if($total > 5000)
                <div class="cart__delivery-price">
                    <h3 class="cart__delivery-price-title">Сумма доставки:</h3>
                    <span class="dost">0</span>
                </div>
                <div class="cart__all-price">
                    <h3 class="cart__all-price-title">Сумма заказа:</h3>
                    <span class="cart__all-price-title-main">{{ $total }}</span>
                </div>
                <div class="cart__all-price">
                    <h3 class="cart__all-price-title">Итого:</h3>
                    <span class="cart__all-price-title-main">{{ $total }}</span>
                </div>

                @else
                <div class="cart__delivery-price">
                    <h3 class="cart__delivery-price-title">Сумма доставки:</h3>
                    <span class="dost">300</span>
                </div>
                <div class="cart__all-price">
                    <h3 class="cart__all-price-title">Сумма заказа:</h3>
                    <span class="cart__all-price-title-main">{{ $total }}</span>
                </div>
                <div class="cart__all-price">
                    <h3 class="cart__all-price-title">Итого:</h3>
                    <span class="cart__all-price-title-main">{{ $total + 300 }}</span>
                </div>

                @endif
                <div class="order-foot__send"></div>
            </div>
        </div>
        <div class="tab-pane " id="contact" role="tabpanel" aria-labelledby="contact-tab">
            <div class="cart--delivery__spb-container">
                <p class="text-delivery">Доставка по России осуществляется транспортной компанией «СДЭК»<br> до
                    ближайшего пункта выдачи заказов.</p>
                <div class="cart-form__item required">
                    <div class="form-warning cart-form__warning">Введите адрес доставки (улица, дом, квартира)</div>
                    <label for="" class="cart-form__label">Введите адрес доставки (улица, дом, квартира)</label>
                    <input name="name" id="addressInput" type="text" class="cart-form__input">
                    <ul id="suggestionsList" class="cart-form__address-lines address-lines list-unstyled"></ul>
                </div>
                <div class="input__wrapper"></div>
                <div class="loader d-none">
                    <div class="loading">
                        <svg width="64px" height="48px">
                            <polyline points="0.157 23.954, 14 23.954, 21.843 48, 43 0, 50 24, 64 24" id="back"></polyline>
                            <polyline points="0.157 23.954, 14 23.954, 21.843 48, 43 0, 50 24, 64 24" id="front"></polyline>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
