<div class="cart-order @if(empty($products))no_color @endif" data-safe="1">
    @if($products)
    <h2>Ваш заказ</h2>
    <div class="cart-order__wrapper">
        <div class="cart-order__nat">
            @foreach ($products as $product)
            <div class="cart-order__item cart-order__item_nat cart-order__item_1" data-id="1">

                <div class="cart-order__item__img-container">
                    @if (count($product->img()) > 0)
                    <img src="{{ $product->img()[0] }}" alt="{{ $product->title }}">
                    @else
                    <img src="{{ asset('assets/img/no-image.png') }}" class="contain" alt="{{ $product->title }}">
                    @endif
                </div>

                <div class="card-order__item_wrapper">
                    <div class="cart-order__info">
                        <a href="{{ route('catalog.product',['product'=>$product->url]) }}">
                            <h5 class="cart-order__title">{{ $product->title }}
                            </h5>
                            <p class="cart-gramm">Вес:
                                100
                                г</p>
                        </a>
                        <div class="cart-order__price">{{ $product->price }}</div>
                    </div>
                    <div class="cart-order__foot">
                        <button class="btn button-cart cart-order__del rem_cart" data-id="{{ $product->id }}">
                            ╳
                            <span>Удалить</span>
                        </button>
                        <div class="cart-order__counter counter">
                            <button class="counter__button button button-arrow-inner del_cart" data-action="decrease-count" data-id="{{ $product->id }}">
                                <span class="minus">-</span>
                            </button>
                            <span class="counter__count">{{ $product->count }}</span>
                            <button class="counter__button button button-arrow-inner add_cart" data-action="increase-count" data-id="{{ $product->id }}">
                                <span class="plus">+</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="cart-order-go">
                <div class="cart__all-price">
                    Итого:
                    <span>{{ $total }}</span>
                </div>
            </div>

        </div>
    </div>
    <x-catalog::front.cart_delivery :total="$total"/>
    <section class="order col-12 order-3">
        <div class="order-head">
            <h2>Контактная информация</h2>
        </div>
        <div class="order-body">
            <form action="" method="POST" id="order_send" class="cart-form">
                <div class="row">
                    <div class="cart-form__item required col-xl-6">
                        <div class="form-warning cart-form__warning">Выберите физ лицо или юр лицо</div>
                        <label for="" class="cart-form__label">Выберите тип лица:</label>
                        <select class='select-control' name="lico" id="lico">
                            <option value="fiz-lico">Физическое лицо</option>
                            <option value="ur-lico">Юридическое лицо</option>
                        </select>
                    </div>
                    <div class="cart-form__item required col-xl-6">
                        <div class="form-warning cart-form__warning">Введите ваше имя</div>
                        <label for="" class="cart-form__label">Контакное лицо</label>
                        <input name="name" type="text" class="cart-form__input">
                    </div>

                    <div class="cart-form__item col-xl-6 required">
                        <label for="" class="cart-form__label">Email</label>
                        <input name="email" type="email" class="cart-form__input">
                    </div>
                    <div class="cart-form__item required col-xl-6">
                        <div class="form-warning cart-form__warning">Введите ваш номер телефона</div>
                        <label for="" class="cart-form__label">Телефон</label>
                        <input name="phone" type="tel" class="cart-form__input tel" inputmode="text">
                    </div>

                    <div class="cart-form__item">
                        <textarea class="cart-form__textarea" name="comment" id="" placeholder="Комментарий"></textarea>
                    </div>
                </div>
            </form>
        </div>
        <div class="cart__messages " id="cart-message">Заполните обязательные поля</div>

        <div class="order-foot">

            <div class="consent">
                <div class="consent__checkbox_wrapper">
                    <input class="consent__checkbox" type="checkbox" id="consent_checkbox" name="confident" checked="" form="order_send">
                    <label for="consent_checkbox" class="checkbox-label"></label>
                </div>
                <label class="consent__text">
                    <span>Нажимая кнопку Оформить заказ вы соглашаетесь с</span>
                    <br>
                    <a href="/privacy/" target="_blank">условиями обработки персональных данных.</a>
                </label>
            </div>

            <div class="order-foot__send">
                <button type="submit" form="order_send" name="send" value="1" class="send-order button ">
                    Оплатить заказ
                </button>
            </div>
        </div>
    </section>
    @else
    <div class="empty-cart">
        <p>Корзина пуста</p>
        <a href="{{ route('catalog') }}" class="button">Перейти в каталог</a>
    </div>
    @endif
</div>
@vite('resources/js/catalog/cart/form.js')
