<form id="filter_form" data-category="{{ $category->url }}">
    <div class="catalog-category__control">
        <x-catalog::front.filters :category="$category" />
    </div>
</form>
<div class="message-search d-none">
    <span>Найдено 2 товара</span>
</div>

@vite('resources/js/catalog/filter_prod.js')
