<div class="row info-row">
    <div class="col-xl-4">
        <div class="product-gallery">
            @if ($product->images && count($product->images) > 1)
            <div class="swiper productInfo-slider">
                <div class="swiper-wrapper">
                    @foreach ($product->images as $image)
                    <div class="swiper-slide">
                        <div class="item">
                            <a data-fancybox="gallery" href="{{ $image }}">
                                <img src="{{ $image }}" alt="">
                                <div class="product-labels__item-container product-labels__item-container-dop ">
                                    @if($product->new)
                                    <div class="product-labels__item product-labels__item-new">new</div>
                                    @endif
                                    @if($product->hit)
                                    <div class="product-labels__item-hit product-labels__item">hit</div>
                                    @endif
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div thumbsSlider="" class="swiper productInfo__thumb-slider">
                <div class="swiper-wrapper">
                    @foreach ($product->images as $image)
                    <div class="swiper-slide">
                        <div class="item">
                            <img src="{{ $image }}" alt="">
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @elseif($product->images && count($product->images) > 0)

            <div class="no-slider full">
                <div class="item">
                    <a data-fancybox="gallery" href="{{ $product->images[0] }}" class="img-container">
                        <img src="{{ $product->images[0] }}" alt="">
                        <div class="product-labels__item-container product-labels__item-container-dop ">
                            @if($product->new)
                            <div class="product-labels__item product-labels__item-new">new</div>
                            @endif
                            @if($product->hit)
                            <div class="product-labels__item-hit product-labels__item">hit</div>
                            @endif
                        </div>
                    </a>
                </div>
            </div>
            @else
            <div class="no-slider">
                <div class="item">
                    <div class="img-container">
                        <a data-fancybox="gallery" href="{{ asset('assets/img/no-image.png') }}">
                            <img class="no-image" src="{{ asset('assets/img/no-image.png') }}" alt="">
                            <div class="product-labels__item-container product-labels__item-container-dop ">
                                @if($product->new)
                                <div class="product-labels__item product-labels__item-new">new</div>
                                @endif
                                @if($product->hit)
                                <div class="product-labels__item-hit product-labels__item">hit</div>
                                @endif
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="col-xl-5 shop__content-container">
        <div class="shop-card__content">
            <div class="tabs__item active" id="tab-1">
                <p><span>Подходит для индивидуальной и групповой работы</span></p>
                <p><span>Позволяет составлять различные комбинации столов</span></p>
                <p><span>Столешница ЛДСП&nbsp;</span></p>
                <p>Металлокаркас</p>
                <p>Цвет по выбору заказчика</p>
                <p>Высота опоры 735 мм</p>
            </div>
        </div>
        <hr style="margin-left: 30px;">
        <div class="card__desc">
            <div class="card__desc__item itemList-svg itemList">
                <div class="card__desc__item-icon">
                    <svg width="18" height="17" viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M16.1327 1.81637L8.49341 9.45571L5.32137 6.28576C5.05546 6.01985 4.62288 6.01985 4.35696 6.28576C4.09105 6.55167 4.09105 6.98425 4.35696 7.25017L8.01122 10.9044C8.14531 11.0385 8.3183 11.1034 8.49341 11.1034C8.66851 11.1034 8.84151 11.0364 8.97559 10.9044L17.0991 2.78282C17.365 2.51691 17.365 2.08434 17.0991 1.81842C16.831 1.55025 16.4008 1.55025 16.1326 1.81615L16.1327 1.81637Z" fill="#942238"></path>
                        <path d="M8.5 1.36454C10.219 1.36454 11.7953 1.97855 13.0299 2.99485L13.9987 2.02608C12.5176 0.763355 10.6018 0 8.5 0C3.81407 0.000150818 0 3.81442 0 8.5C0 13.1856 3.81427 17 8.5 17C13.1855 17 16.9934 13.1857 16.9934 8.5C16.9934 7.40802 16.788 6.36581 16.4075 5.40368L15.3372 6.47396C15.5275 7.11619 15.6313 7.7973 15.6313 8.50004C15.6313 12.4354 12.4312 15.6355 8.5024 15.6355C4.56709 15.6355 1.36694 12.4354 1.36694 8.50004C1.36468 4.56472 4.56477 1.36458 8.50009 1.36458L8.5 1.36454Z" fill="#942238"></path>
                    </svg>
                </div>
                <div class="card__desc__item-info">
                    <p>Изготовим мебель под заказ по Вашим цветам и размерам</p>
                </div>
            </div>
            <div class="card__desc__item itemList-svg itemList">
                <div class="card__desc__item-icon">
                    <svg width="18" height="17" viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M16.1327 1.81637L8.49341 9.45571L5.32137 6.28576C5.05546 6.01985 4.62288 6.01985 4.35696 6.28576C4.09105 6.55167 4.09105 6.98425 4.35696 7.25017L8.01122 10.9044C8.14531 11.0385 8.3183 11.1034 8.49341 11.1034C8.66851 11.1034 8.84151 11.0364 8.97559 10.9044L17.0991 2.78282C17.365 2.51691 17.365 2.08434 17.0991 1.81842C16.831 1.55025 16.4008 1.55025 16.1326 1.81615L16.1327 1.81637Z" fill="#942238"></path>
                        <path d="M8.5 1.36454C10.219 1.36454 11.7953 1.97855 13.0299 2.99485L13.9987 2.02608C12.5176 0.763355 10.6018 0 8.5 0C3.81407 0.000150818 0 3.81442 0 8.5C0 13.1856 3.81427 17 8.5 17C13.1855 17 16.9934 13.1857 16.9934 8.5C16.9934 7.40802 16.788 6.36581 16.4075 5.40368L15.3372 6.47396C15.5275 7.11619 15.6313 7.7973 15.6313 8.50004C15.6313 12.4354 12.4312 15.6355 8.5024 15.6355C4.56709 15.6355 1.36694 12.4354 1.36694 8.50004C1.36468 4.56472 4.56477 1.36458 8.50009 1.36458L8.5 1.36454Z" fill="#942238"></path>
                    </svg>
                </div>
                <div class="card__desc__item-info">
                    <p>Используем только качественные и безопасные материалы</p>
                </div>
            </div>
            <div class="card__desc__item itemList-svg itemList">
                <div class="card__desc__item-icon">
                    <svg width="18" height="17" viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M16.1327 1.81637L8.49341 9.45571L5.32137 6.28576C5.05546 6.01985 4.62288 6.01985 4.35696 6.28576C4.09105 6.55167 4.09105 6.98425 4.35696 7.25017L8.01122 10.9044C8.14531 11.0385 8.3183 11.1034 8.49341 11.1034C8.66851 11.1034 8.84151 11.0364 8.97559 10.9044L17.0991 2.78282C17.365 2.51691 17.365 2.08434 17.0991 1.81842C16.831 1.55025 16.4008 1.55025 16.1326 1.81615L16.1327 1.81637Z" fill="#942238"></path>
                        <path d="M8.5 1.36454C10.219 1.36454 11.7953 1.97855 13.0299 2.99485L13.9987 2.02608C12.5176 0.763355 10.6018 0 8.5 0C3.81407 0.000150818 0 3.81442 0 8.5C0 13.1856 3.81427 17 8.5 17C13.1855 17 16.9934 13.1857 16.9934 8.5C16.9934 7.40802 16.788 6.36581 16.4075 5.40368L15.3372 6.47396C15.5275 7.11619 15.6313 7.7973 15.6313 8.50004C15.6313 12.4354 12.4312 15.6355 8.5024 15.6355C4.56709 15.6355 1.36694 12.4354 1.36694 8.50004C1.36468 4.56472 4.56477 1.36458 8.50009 1.36458L8.5 1.36454Z" fill="#942238"></path>
                    </svg>
                </div>
                <div class="card__desc__item-info">
                    <p>Цена рассчитывается с учётом размера и дизайна</p>
                </div>
            </div>
            <div class="card__desc__item itemList-svg itemList">
                <div class="card__desc__item-icon">
                    <svg width="18" height="17" viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M16.1327 1.81637L8.49341 9.45571L5.32137 6.28576C5.05546 6.01985 4.62288 6.01985 4.35696 6.28576C4.09105 6.55167 4.09105 6.98425 4.35696 7.25017L8.01122 10.9044C8.14531 11.0385 8.3183 11.1034 8.49341 11.1034C8.66851 11.1034 8.84151 11.0364 8.97559 10.9044L17.0991 2.78282C17.365 2.51691 17.365 2.08434 17.0991 1.81842C16.831 1.55025 16.4008 1.55025 16.1326 1.81615L16.1327 1.81637Z" fill="#942238"></path>
                        <path d="M8.5 1.36454C10.219 1.36454 11.7953 1.97855 13.0299 2.99485L13.9987 2.02608C12.5176 0.763355 10.6018 0 8.5 0C3.81407 0.000150818 0 3.81442 0 8.5C0 13.1856 3.81427 17 8.5 17C13.1855 17 16.9934 13.1857 16.9934 8.5C16.9934 7.40802 16.788 6.36581 16.4075 5.40368L15.3372 6.47396C15.5275 7.11619 15.6313 7.7973 15.6313 8.50004C15.6313 12.4354 12.4312 15.6355 8.5024 15.6355C4.56709 15.6355 1.36694 12.4354 1.36694 8.50004C1.36468 4.56472 4.56477 1.36458 8.50009 1.36458L8.5 1.36454Z" fill="#942238"></path>
                    </svg>
                </div>
                <div class="card__desc__item-info">
                    <p>Изображение продукции может отличаться от оригинала</p>
                </div>
            </div>
            <div class="card__desc__item itemList-svg itemList">
                <div class="card__desc__item-icon">
                    <svg width="18" height="17" viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M16.1327 1.81637L8.49341 9.45571L5.32137 6.28576C5.05546 6.01985 4.62288 6.01985 4.35696 6.28576C4.09105 6.55167 4.09105 6.98425 4.35696 7.25017L8.01122 10.9044C8.14531 11.0385 8.3183 11.1034 8.49341 11.1034C8.66851 11.1034 8.84151 11.0364 8.97559 10.9044L17.0991 2.78282C17.365 2.51691 17.365 2.08434 17.0991 1.81842C16.831 1.55025 16.4008 1.55025 16.1326 1.81615L16.1327 1.81637Z" fill="#942238"></path>
                        <path d="M8.5 1.36454C10.219 1.36454 11.7953 1.97855 13.0299 2.99485L13.9987 2.02608C12.5176 0.763355 10.6018 0 8.5 0C3.81407 0.000150818 0 3.81442 0 8.5C0 13.1856 3.81427 17 8.5 17C13.1855 17 16.9934 13.1857 16.9934 8.5C16.9934 7.40802 16.788 6.36581 16.4075 5.40368L15.3372 6.47396C15.5275 7.11619 15.6313 7.7973 15.6313 8.50004C15.6313 12.4354 12.4312 15.6355 8.5024 15.6355C4.56709 15.6355 1.36694 12.4354 1.36694 8.50004C1.36468 4.56472 4.56477 1.36458 8.50009 1.36458L8.5 1.36454Z" fill="#942238"></path>
                    </svg>
                </div>
                <div class="card__desc__item-info">
                    <p>Предложение не является публичной офертой</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3">
        <div class="card__inner">
            <span class="card-price card-price-1348">{{ $product->price }}</span>
            <button class="button button_s button-bag add_cart" data-product="" data-id="{{ $product->id }}">
                В корзину
            </button>
            <button class="button button_w button-click one-click-buy" data-type='oneClick' data-img='{{ $product->images ? $product->images[0] : asset('assets/img/no-image.png') }}' data-formname='Купить в один клик' data-id='{{ $product->id }}' data-name='{{ $product->title }}' data-id="{{ $product->id }}">Купить в один клик</button>
        </div>
        <div class="card__info">
            <div class="card__info__item itemList-svg itemList">
                <div class="card__info__item-icon">
                    <svg width="19" height="17" viewBox="0 0 19 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M18.3941 4.04762L15.9431 0H2.45102L0 4.04762V17H18.3942L18.3941 4.04762ZM17.1573 3.73281H11.648L11.1309 0.899399H15.4258L17.1573 3.73281ZM7.57792 4.6322H10.8161V8.90469H7.55552L7.57792 4.6322ZM7.6678 3.73281L8.18496 0.899519H10.2537L10.7709 3.73281H7.6678ZM2.96807 0.899399H7.26304L6.74588 3.73281H1.25911L2.96807 0.899399ZM0.899282 16.1005V4.6322H6.67838V9.80416H11.7154V4.6322H17.4945V16.1005H0.899282Z" fill="#942238"></path>
                        <path d="M2.54095 14.8413H6.70101V11.648H2.54095V14.8413ZM3.44035 12.5476H5.80166V13.9643L3.44051 13.9641L3.44035 12.5476Z" fill="#942238"></path>
                    </svg>
                </div>
                <div class="card__info__item-info">
                    <p>Доставка по всей России</p>
                </div>
            </div>
            <div class="card__info__item itemList-svg itemList">
                <div class="card__info__item-icon">
                    <svg width="15" height="17" viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13.4376 2.66C10.9376 2.66 9.03765 1.94 7.45765 0.399964L7.03772 0L6.61765 0.399964C5.03773 1.94 3.13773 2.66 0.63766 2.66H0.0376253V3.26003C0.0376253 3.6801 0.0376257 4.10003 0.0176761 4.54003C-0.0624024 8.76002 -0.162431 14.5 6.83767 16.94L7.03772 17L7.23778 16.94C14.2378 14.5 14.1378 8.76002 14.0578 4.54003C14.0578 4.10003 14.0378 3.66 14.0378 3.26003V2.66H13.4376ZM7.03765 15.74C1.05766 13.56 1.13766 8.77999 1.21766 4.57999C1.21766 4.34004 1.21766 4.09996 1.23761 3.85999C3.57761 3.75997 5.4376 3.06007 7.0376 1.65993C8.63763 3.05989 10.4976 3.75996 12.8376 3.85999C12.8376 4.09995 12.8376 4.34003 12.8575 4.57999C12.9378 8.77999 13.0178 13.56 7.03791 15.74H7.03765Z" fill="#942238"></path>
                        <path d="M6.17766 9.07997L4.71756 7.46001L3.83752 8.26008L6.11752 10.8L10.2575 6.87997L9.43748 5.99994L6.17766 9.07997Z" fill="#942238"></path>
                    </svg>
                </div>
                <div class="card__info__item-info">
                    <p>Гарантия на продукцию</p>
                </div>
            </div>
        </div>
    </div>
</div>

@vite('resources/js/catalog/product_info.js')
