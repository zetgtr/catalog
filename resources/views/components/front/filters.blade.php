@foreach ($categories_filters as $categoryFilter)
    @php
        $filters = $categoryFilter
            ->filter()
            ->whereHas('products', function ($product) use ($category) {
                return $product->whereHas('categories', function ($q) use ($category) {
                    return $q->where('id', $category->id);
                });
            })
            ->orderBy('name')
            ->get();
        $categoryFilter->filters = $filters;
    @endphp
@endforeach
@foreach ($categories_filters as $categoryFilter)
    @if (!$categoryFilter->hidden)
        @php
            $filters = $categoryFilter->filters
        @endphp
        <div class="accordion-item accordion-item_desktop">
            <div class="accordion-header" id="heading-{{ $categoryFilter->id }}">
                <button class="accordion-button @if ($categoryFilter->open !== 'on') collapsed @endif " type="button"
                        data-bs-toggle="collapse" data-bs-target="#collapse-{{ $categoryFilter->id }}" aria-expanded="false"
                        aria-controls="collapse-{{ $categoryFilter->id }}">
                    <span class="accordion-title link-line">{{ $categoryFilter->name }}</span>
                    <span class="accordion-icon">
            </span>
                </button>
            </div>
            <div id="collapse-{{ $categoryFilter->id }}"
                 class="accordion-collapse collapse @if ($categoryFilter->open == 'on') show @endif collapse-main"
                 aria-labelledby="heading-{{ $categoryFilter->id }}">
                <div class="accordion-body block-scroll-wrap custom-scroll">
                    @foreach ($filters->sortBy('name') as $filter)
                        @if ($categoryFilter->type === 'slider-range')
                            @php
                                $range = !empty($filtersRequest['range'][$filter->alias])
                                ? $filtersRequest['range'][$filter->alias]
                                : null;
                            @endphp
                            <div class="ware-filter__item">
                                <div class="irs--wrap">
                                    <input type="text" class="js-range-slider filter-range" name="price_range"
                                           value="" data-type="double"
                                           data-min="{{ $filter->categoryFiltersValue($category)->min }}"
                                           data-max="{{ $filter->categoryFiltersValue($category)->max }}"
                                           data-from="{{ $range['min'] ?? $filter->categoryFiltersValue($category)->min }}"
                                           data-to="{{ $range['max'] ?? $filter->categoryFiltersValue($category)->max }}"
                                           data-grid="false" data-alias="{{ $filter->alias }}" />
                                </div>
                            </div>
                        @elseif($categoryFilter->type === 'checkbox')
                            <div class="ware-filter-input ware-filter-input_ch">
                                <label class="checkbox__label ware-filter__label">
                                    <input id="{{ $filter->alias }}" data-category="{{ $categoryFilter->alias }}"
                                           @checked(!empty($filtersRequest['checkbox'][$filter->alias]) && $filtersRequest['checkbox'][$filter->alias]) type="checkbox" name="{{ $filter->alias }}"
                                           value="1" class="checkbox collapsed-input filter-check">
                                    <span class="checkbox__item ware-filter__check"><x-front.icons.check /></span>
                                    <span class="checkbox__desc ware-filter-title link-line">{{ $filter->name }}</span>
                                </label>
                            </div>
                        @elseif($categoryFilter->type === 'property')
                            <div class="ware-filter__item ware-filter-input_select">
                                <select id="filter-{{ $categoryFilter->alias }}" name="{{ $categoryFilter->alias }}" class="ware-filter-select">
                                    <option value="">{{ __('Выберите значение') }}</option>
                                    @foreach (array_unique($properties[$categoryFilter->alias]) as $property)
                                        <option value="{{ $property }}"
                                                @if (!empty($filtersRequest[$categoryFilter->alias][$property]) && $filtersRequest[$categoryFilter->alias][$property] == $property)
                                                    selected
                                            @endif>
                                            {{ $property }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    @endif
@endforeach

<div class="accordion-item accordion-item_mobile">
    <div class="accordion-header" id="heading-mobile">
        <button class="accordion-button @if ($categoryFilter->open !== 'on') collapsed @endif " type="button"
                data-bs-toggle="collapse" data-bs-target="#collapse-mobile" aria-expanded="false"
                aria-controls="collapse-mobile">
            <span class="accordion-title link-line">Фильтры</span>
            <span class="accordion-icon">
            </span>
        </button>
    </div>
    <div id="collapse-mobile"
         class="accordion-collapse collapse @if ($categoryFilter->open == 'on') show @endif collapse-main"
         aria-labelledby="heading-mobile" data-bs-parent="#heading-mobile">
        <div class="accordion-body block-scroll-wrap">
            @foreach ($categories_filters as $categoryFilter)
                @if (!$categoryFilter->hidden)
                    @php
                        $filters = $categoryFilter->filters
                    @endphp
                    <div class="accordion-item">
                        <div class="accordion-header accordion-header_mobile"
                             id="heading-mobile-{{ $categoryFilter->id }}">
                            <button class="accordion-button @if ($categoryFilter->open !== 'on') collapsed @endif "
                                    type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse-mobile-{{ $categoryFilter->id }}" aria-expanded="false"
                                    aria-controls="collapse-mobile-{{ $categoryFilter->id }}">
                                <span class="accordion-title link-line">{{ $categoryFilter->name }}</span>
                                <span class="accordion-icon"></span>
                            </button>
                        </div>
                        <div id="collapse-mobile-{{ $categoryFilter->id }}"
                             class="accordion-collapse accordion-collapse_mobile collapse @if ($categoryFilter->open == 'on') show @endif"
                             aria-labelledby="heading-mobile-{{ $categoryFilter->id }}">
                            <div class="accordion-body block-scroll-wrap custom-scroll">
                                @foreach ($filters->sortBy('name') as $filter)
                                    @if ($categoryFilter->type === 'slider-range')
                                        @php
                                            $range = !empty(request('range')[$filter->alias])
                                            ? request('range')[$filter->alias]
                                            : null;
                                        @endphp
                                        <div class="ware-filter__item">
                                            <div class="irs--wrap">
                                                <input type="text" class="js-range-slider filter-range"
                                                       name="price_range" value="" data-type="double"
                                                       data-min="{{ $filter->categoryFiltersValue($category)->min }}"
                                                       data-max="{{ $filter->categoryFiltersValue($category)->max }}"
                                                       data-from="{{ $range['min'] ?? $filter->categoryFiltersValue($category)->min }}"
                                                       data-to="{{ $range['max'] ?? $filter->categoryFiltersValue($category)->max }}"
                                                       data-grid="false" data-alias="{{ $filter->alias }}" />
                                            </div>
                                        </div>
                                    @elseif($categoryFilter->type === 'checkbox')
                                        <div class="ware-filter-input ware-filter-input_ch">
                                            <label class="checkbox__label ware-filter__label">
                                                <input id="{{ $filter->alias }}" @checked(!empty($filtersRequest['checkbox'][$filter->alias]) && $filtersRequest['checkbox'][$filter->alias])
                                                type="checkbox" name="{{ $filter->alias }}" value="1"
                                                       class="checkbox collapsed-input filter-check">
                                                <span class="checkbox__item ware-filter__check">
                                    <x-front.icons.check />
                                </span>
                                                <span class="checkbox__desc ware-filter-title link-line">
                                    {{ $filter->name }}</span>
                                            </label>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>
<div class="filters__btn">
    <button type="button" class="filters__btn-apply">Применить</button>
    <button type="button" class="filters__btn-reset">Сбросить</button>
</div>


<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>
