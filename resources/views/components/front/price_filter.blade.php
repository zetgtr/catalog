<div class="ware-filter filters__price ware-filter_price ware-filter_range">
    <div class="ware-filter__body collapse show" id="price" aria-labelledby="head_price">
        <div class="accordion">
            <div class="block-scroll-wrap_noscroll range-filter">
                <div class="flt__item">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingPrice">
                            <button class="accordion-button @if($priceMinRequest) @else collapsed  @endif" type="button" data-bs-toggle="collapse" data-bs-target="#collapsePrice" aria-expanded="false" aria-controls="collapsePrice">
                                Цена
                                <div class="btn-container">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128">
                                        <path d="M31.82886,125.94216a3.576,3.576,0,0,0,2.51832,1.05769,3.45014,3.45014,0,0,0,2.51832-1.05769L96.24741,66.56025a3.53825,3.53825,0,0,0,.03292-5.00372l-.03292-.03292L36.8655,2.12911a3.56342,3.56342,0,0,0-5.20463,4.86864q.08118.08678.168.168l56.8636,56.87619-56.8636,56.8636a3.55084,3.55084,0,0,0-.015,5.02161Z"></path>
                                    </svg>
                                </div>
                            </button>
                        </h2>
                        <div id="collapsePrice" class="accordion-collapse collapse @if($priceMinRequest) show @endif" aria-labelledby="headingPrice">
                            <div class="accordion-body">
                                <div class="ware-filter__item">
                                    <div class="irs--wrap">
                                        <input type="text" class="js-range-slider filter-range" name="my_range" value=""
                                        data-type="double"
                                        data-min="{{ $priceMin }}"
                                        data-max="{{ $priceMax }}"
                                        data-from="{{ $priceMinRequest }}"
                                        data-to="{{ $priceMaxRequest }}"
                                        data-grid="false"
                                    />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>
{{-- @vite('resources/js/catalog/irs.js') --}}
