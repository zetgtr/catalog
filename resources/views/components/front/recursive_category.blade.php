<ul class="subcategories list-reset">
    @foreach ($categories as $category)
        <li><a class='ware-filter-input ware-filter-input_ch' href="{{ route('catalog.category', ['category' => $category->url]) }}">{{ $category->title }}</a></li>
    @endforeach
</ul>
