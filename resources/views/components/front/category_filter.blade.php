<div class="ware-filter filters__price ware-filter_price ware-filter_range category-filter">
    <div class="ware-filter__body collapse show" id="price" aria-labelledby="head_price">
        <div class="accordion">
            <div class="block-scroll-wrap_noscroll range-filter">
                <div class="flt__item">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingPrice">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseCategory" aria-expanded="false" aria-controls="collapseCategory">
                                Категория
                                <div class="btn-container">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128">
                                        <path d="M31.82886,125.94216a3.576,3.576,0,0,0,2.51832,1.05769,3.45014,3.45014,0,0,0,2.51832-1.05769L96.24741,66.56025a3.53825,3.53825,0,0,0,.03292-5.00372l-.03292-.03292L36.8655,2.12911a3.56342,3.56342,0,0,0-5.20463,4.86864q.08118.08678.168.168l56.8636,56.87619-56.8636,56.8636a3.55084,3.55084,0,0,0-.015,5.02161Z"></path>
                                    </svg>
                                </div>
                            </button>
                        </h2>
                        <div id="collapseCategory" class="accordion-collapse collapse show" aria-labelledby="headingPrice">
                            <div class="accordion-body">
                                @foreach ($categories as $category)
                                    <a href="{{ route('catalog.category', ['category' => $category->url]) }}" class="ware-filter-input ware-filter-input_ch">{{ $category->title }}</a>
                                    @if ($category->parent)
                                        <x-catalog::front.recursive_category :categories='$category->parent' />
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
