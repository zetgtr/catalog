@if(count($products))
<div class="catalog__wrapper-products">
    @foreach ($products as $product)
        <x-catalog::front.utils.popular :product='$product' :index="$loop->index"/>
    @endforeach
</div>
<div class="pagination-catalog">
    {{ $products->withPath($path.(request('url') ?? $url))->appends(request()->input())->links() }}
</div>
@else
<div class="catalog__wrapper-products catalog__wrapper-products-empty">
    <p>Не найдено товаров!</p>
</div>
@endif
