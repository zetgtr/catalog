@php
$hrClasses = [];

// Добавляем классы в массив в зависимости от индекса
if (($index + 1) % 3 == 0) {
$hrClasses[] = 'line-xl';
}
if (($index + 1) % 2 == 0) {
$hrClasses[] = 'line-l';
}
if (($index + 1) % 1 == 0) {
$hrClasses[] = 'line-sm';
}
if (($index + 1) == 1){
    $hrClasses[] = 'line-first';
}
@endphp
<div class="product">
    <div class="img-wrapepr">
        <a href='{{ url('catalog.product',['product'=>$product->url]) }}' class="img-container">
            @php
            $images = is_array($product->images) ? $product->images : ($product->images);
            @endphp
            @if (is_array($images) && count($images) > 0)
            <img src="{{ $images[0] }}" alt="{{ $product->title }}">
            @else
            <img src="{{ asset('assets/img/no-image.png') }}" class="contain" alt="{{ $product->title }}">
            @endif
        </a>
        <div class="product-labels">
            @if ($product->new == 1)
            <div class="product-labels__item">new</div>
            @endif

            @if ($product->hit == 1)
            <div class="product-labels__item">hit</div>
            @endif
        </div>
    </div>
    <div class="content">
        <a href="{{ url('catalog.product',['product'=>$product->url]) }}">
            <h4 class="title">{{ $product->title }}</h4>
        </a>
        <div class="desc">
            <p class="description">{!! $product->content !!}</p>
        </div>
        <div class="bottom">
            <div class="price">{{ number_format($product->price, 0, '.', ' ') }}<span>₽</span></div>
            <button class="button button-s button-product add_cart"  data-product="" data-id="{{ $product->id }}">В корзину</button>
        </div>
    </div>
</div>
@if (!empty($hrClasses))
<hr class="line-hr {{ implode(' ', $hrClasses) }} d-none">
@endif
