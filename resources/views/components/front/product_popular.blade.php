<div class="product__popular">
    <div class="product__popular__header">
        <h2 class="product__popular-title">С этим товаром смотрят</h2>
    </div>
    <input type="hidden" class="countPopularSlides" value='{{ count($popular) }}'>
    <div class="swiper popular-slider">
        <div class="swiper-wrapper">

            @php
                $limitedItems = $popular->take(2);
            @endphp
            @foreach ($popular as $item)
                <div class="swiper-slide">
                    <div class="slide__content">
                        <div class="slide__content-up">
                            <div class="img__wrapper">
                                <a href='{{ url('catalog', ['category' => $item->categories->first()->url, 'product' => $item->url]) }}' class="img-container">
                                    @php
                                        $images = is_array($item->images) ? $item->images : json_decode($item->images, true);
                                    @endphp
                                    @if (is_array($images) && count($images) > 0)
                                        <img src="{{ $images[0] }}" alt="{{ $item->title }}">
                                    @else
                                        <img src="{{ asset('assets/img/no-image.png') }}" class="contain" alt="{{ $item->title }}">
                                    @endif
                                </a>
                            </div>
                            <div class="descr__wrapper">
                                <span class="descr__wrapper-nameProduct">{{ $item->title }}</span>
                                <span class="descr__wrapper-status">В наличии</span>
                            </div>
                        </div>

                        <div class="orderProcess">
                            <span class="orderProcess__price">{{ $item->price }} ₽</span>
                            <div class="orderProcess__button">
                                <div class="buyOneClick">
                                    <x-front.icons.buyOneClick />
                                    <span class="text__btn">Купить в 1 клик</span>
                                </div>
                                <div class="cart__btn popular__btn add_cart" data-product="" data-id="5">
                                    <x-front.icons.cart />
                                    <span class="text__btn">В корзину</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="swiper__button-controller">
            <div class="arrowSlider popular-slider-prev">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" width="10"
                     height="15">
                    <path
                        d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l192 192c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L77.3 256 246.6 86.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-192 192z" />
                </svg>
            </div>
            <div class="arrowSlider popular-slider-next">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" width="10"
                     height="15">
                    <path
                        d="M310.6 233.4c12.5 12.5 12.5 32.8 0 45.3l-192 192c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L242.7 256 73.4 86.6c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l192 192z" />
                </svg>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>
@vite('resources/js/catalog/popular.js')
