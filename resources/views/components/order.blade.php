<table id="example2" class="table table-bordered text-nowrap border-bottom">
    <thead>
        <tr>
            <th class="border-bottom-0 text-center">ID</th>
            <th class="border-bottom-0 text-center">Заказчик</th>
            <th class="border-bottom-0 text-center">Заказ</th>
            <th class="border-bottom-0 text-center">Дата/Время</th>
        </tr>
    </thead>
    <tbody>
        @foreach($orders as $order)
        <tr class="delete-element">
            <td class="text-center">{{ $order->id }}</td>
            <td>
                <div>
                    <strong>Контактное лицо:</strong>
                    {{ $order->name }}
                </div>
                <div>
                    <strong>Телефон:</strong>
                    <a href="{{ $order->phone }}">{{ $order->phone }}</a>
                </div>
                <div>
                    <strong>Email:</strong>
                    <a href="mailto:{{ $order->email }}">{{ $order->email }}</a>
                </div>
            </td>
            <td class="text-center">
                <table style='width: 100%;' class="order-table__inner">
                    @php
                    $totalSum = 0; // Initialize the total sum variable
                    @endphp
                    <tbody>
                        @foreach ($order->products()->get() as $product)
                        <tr>
                            <td class="text-start">
                                {{ $product->title }}
                            </td>
                            <td style='width: 200px;' class="text-start">
                                {{ $product->pivot->count }} шт.,
                                @if ($product->pivot->price && $product->pivot->price !== 0)
                                {{ $product->pivot->price }}
                                @else
                                {{ $product->price }}
                                @endif
                            </td>
                            <td style='width: 200px;' class="text-start">
                                @php
                                $price = ($product->pivot->price && $product->pivot->price !== 0) ? $product->pivot->price : $product->price;
                                $subtotal = $product->pivot->count * $price;
                                $totalSum += $subtotal;
                                @endphp
                                {{ $subtotal }} руб.
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td class="text-start">
                                <strong>Итого:</strong>
                            </td>
                            <td colspan="2" class="text-start">
                                <strong>{{ $totalSum }} руб.</strong>
                            </td>
                        </tr>
                        @if($order->comment)
                        <tr>
                            <td colspan="3" class="text-start">
                                <div class="mb-2"><strong>Комментарий:</strong></div>
                                <div>{{ $order->comment }}</div>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </td>
            <td class="text-center">{{ $order->created_at->format('d.m.Y H:i') }}</td>
            {{-- <td class="text-center">{{ $feedback->email }}</td>--}}
            {{-- <td class="text-center">{{ $feedback->phone }}</td>--}}
            {{-- <td class="text-center"><a href="{{$feedback->pages}}">{{ $feedback->pages }}</a></td>--}}
        </tr>
        @endforeach
    </tbody>
</table>
<style>
    .order-table__inner td:first-child,
    .order-table__inner td:last-child {
        border: 1px solid #e9edf4 !important;
    }

</style>
<script>
    $('#example2').DataTable({
        responsive: true,
        ordering: false,
        language: {
            searchPlaceholder: 'Поиск...',
            sSearch: '',
            lengthMenu: '_MENU_ Элементы на странице',
        }
    });
</script>
