@extends('layouts.admin')
@section('title',"Каталог")
@section('content')
    <div class="card">
        <x-admin.navigation_list.navigation :links="$links" />
        <div class="card-header">
            <h3 class="card-title">YML</h3>
        </div>
        <div class="card-body">
            <x-warning />
            <div class="row">
                <div class="col-lg-24">
                    @if($settings)
                        <p class="ml-4">
                            Файл сгенерирован: <strong>{{ $settings->updated_at->format('d.m.Y H:i') }}</strong>
                        </p>
                    @endif
                    <a href="{{ route('admin.catalog.yml.create') }}" class="btn btn-orange btn-sm">Сгенирировать</a>
                </div>
                @if($settings)
                    <div class="col-lg-6">
                        <div class="choose-file d-flex align-items-center mt-4">
                            <a download="products.yml" href="/products.yml">
                            <i class="fas fa-file-download" style="font-size: 22px; color: #28a745!important"></i>
                            </a>
                            <input id="link_filed" name="link" type="text" class="form-control form-control-sm mx-3" value="{{ env("APP_URL") }}/products.yml" placeholder="{{ env("APP_URL") }}/products.yml">
                        </div>
                    </div>
                @endif
            </div>


        </div>
    </div>
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.catalog")}}">Каталог</a></li>
            <li class="breadcrumb-item active" aria-current="page">YML</li>
        </ol>
    </div>
@endsection
