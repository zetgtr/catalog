@extends('layouts.admin')
@section('title', 'Каталог')
@section('content')
    <div class="card">
        <x-admin.navigation_list.navigation :links="$links" />
        <div class="card-header d-flex justify-content-between flex-wrap gap-2">
            <div>
                <h3 class="card-title">Товары</h3>
            </div>
            <div class="d-flex justify-content-between flex-wrap gap-2">
                <a data-id="1" data-name="text" class="btn  search modal-effect btn-warning" data-bs-effect="effect-fall"
                    data-bs-toggle="modal" data-bs-target="#modaldemo8" type="button">Поиск</a>
                <a href="{{ route('admin.catalog.product.create') }}" class="btn btn-primary">
                    <i class="fas fa-plus me-1"></i>Добавить позицию
                </a>
            </div>
        </div>
        <div class="card-body">
            <x-catalog::product.index />
        </div>
    </div>
    <x-admin.modal title="Поиск" id="search" />
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <x-catalog::product.seach />
    @vite('resources/js/catalog/search.js')
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Каталог</li>
        </ol>
    </div>
@endsection
