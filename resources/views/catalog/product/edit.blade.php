@extends('layouts.admin')
@section('title',"Каталог")
@section('content')
    <div class="card">
        <x-admin.navigation_list.navigation :links="$links" />
        <div class="card-header">
            <h3 class="card-title">Редактировать товар</h3>
        </div>
        <x-admin.navigation_list.navigatin-js :links="$navigation" />
        <div class="card-body">
            <x-warning />
            <form action="{{ route('admin.catalog.product.update',['product'=>$product]) }}" enctype="multipart/form-data" method="post" class="row row-page-create">
                @csrf
                @method('PUT')
                <input type="hidden" value="{{$product->id}}" name="id">
                <div class="tab-content">
                    <x-catalog::product.edit.content :product="$product" />
                    <x-catalog::product.edit.property :product="$product" />
                    <x-catalog::product.edit.seo :product="$product" />

                    <x-catalog::product.edit.photo :product="$product" />
                </div>
                <div>
                    <input type="submit" value="Сохранить" class="btn btn-success btn-sm">
                </div>
            </form>
        </div>
    </div>
@endsection
