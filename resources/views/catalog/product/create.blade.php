@extends('layouts.admin')
@section('title',"Каталог")
@section('content')
    <div class="card">
        <x-admin.navigation_list.navigation :links="$links" />
        <div class="card-header">
            <h3 class="card-title">Добавить товар</h3>
        </div>
        <x-admin.navigation_list.navigatin-js :links="$navigation" />
        <div class="card-body">
            <x-warning />
            <form action="{{ route('admin.catalog.product.store') }}" enctype="multipart/form-data" method="post" class="row row-page-create">
                @csrf
                <div class="tab-content">
                    <x-catalog::product.create.content />
                    <x-catalog::product.create.seo />
                    <x-catalog::product.create.photo />
                </div>
                <div>
                    <input type="submit" value="Сохранить" class="btn btn-success btn-sm">
                </div>
            </form>
        </div>
    </div>
@endsection
