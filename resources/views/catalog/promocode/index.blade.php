@extends('layouts.admin')
@section('title',"Промокоды")
@section('content')
    <div class="card">
        <x-admin.navigation_list.navigation :links="$links" />
        <div class="card-header">
            <h3 class="card-title">Промокоды</h3>

        </div>
        <div class="card-body">
            <x-warning />
            <div class="row">
                <div class="col-8">
                    <x-catalog::promocode.list />
                </div>
                <div class="col-4">
                    <x-catalog::promocode.add />
                </div>
            </div>

        </div>
    </div>
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.catalog")}}">Каталог</a></li>
            <li class="breadcrumb-item active" aria-current="page">Проиокоды</li>
        </ol>
    </div>
@endsection
