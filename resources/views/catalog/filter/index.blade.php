@extends('layouts.admin')
@section('title',config('catalog.menu_links')[\Catalog\Enums\CatalogEnums::FILTER->value]['name'])
@section('content')
    <div class="card">
        <x-admin.navigation_list.navigation :links="$links" />
        <div class="card-header">
            <h3 class="card-title">Категории</h3>
        </div>
        <div class="card-body">
            <x-warning />
            <x-catalog::filter.category />
        </div>
    </div>
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.catalog")}}">Каталог</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ config('catalog.menu_links')[\Catalog\Enums\CatalogEnums::FILTER->value]['name'] }}</li>
        </ol>
    </div>
@endsection
