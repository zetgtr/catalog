@extends('layouts.admin')
@section('title',config('catalog.menu_links')[\Catalog\Enums\CatalogEnums::FILTER->value]['name'])
@section('content')
    <div class="card">
        <x-admin.navigation_list.navigation :links="$links" />
        <div class="card-header d-flex justify-content-between align-items-center">
            <h3 class="card-title">Категория: {{ $category->name }}</h3>
            <a class="btn btn-info btn-sm" href="{{ route('admin.catalog.filter.index') }}">Назад</a>
        </div>
        <div class="card-body">
            <x-warning />
            <x-catalog::filter.filter :category="$category" />
        </div>
    </div>
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.catalog")}}">Каталог</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.catalog.filter.index")}}">{{ config('catalog.menu_links')[\Catalog\Enums\CatalogEnums::FILTER->value]['name'] }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Категории</li>
        </ol>
    </div>
@endsection
