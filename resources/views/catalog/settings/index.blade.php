@extends('layouts.admin')
@section('title',"Настройки")
@section('content')
    <div class="card">
        <x-admin.navigation_list.navigation :links="$links" />
        <div class="card-header">
            <h3 class="card-title">Настройки</h3>

        </div>
        <div class="card-body">
            <x-catalog::settings />
        </div>
    </div>
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.catalog")}}">Каталог</a></li>
            <li class="breadcrumb-item active" aria-current="page">Настройки</li>
        </ol>
    </div>
@endsection
