@extends('layouts.inner')
@section('title',$seo_title)
@section('description',$seo_description)
@section('keywords',$seo_keywords)
@section('page',true)
@section('content')
<div class="content">
    <div class="catalog__container">
        <div class="container">
            <x-front.elements.breadcrumbs :breadcrumbs="$breadcrumbs" flag='true' />
            <h1>{{ $title }}</h1>
            <div class="catalog @if (!empty($filter)) catalog__wrapper-filter @endif">
                <div class="main">
                    <div class="catalog__wrapper">

                        <x-catalog::front.category :categories="$catalog['categories']" />
                    </div>
                </div>
                @if(!empty($filter))
                    <x-catalog::front.filter />
                @endif
            </div>
        </div>
    </div>
</div>

@endsection
