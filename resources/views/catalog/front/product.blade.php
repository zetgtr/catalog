@extends('layouts.inner')
 @section('title',$seo_title)
@section('description',$seo_description)
@section('keywords',$seo_keywords)
@section('page',true)
@section('content')
<div class="content">
    <div class="container">
        <x-front.elements.breadcrumbs :breadcrumbs="$breadcrumbs" flag='true' />
        <div class="card--box">
            <h1>{{ $product->title }}</h1>
            <x-catalog::front.product_info :product='$product'/>
            <x-catalog::front.product_desc :product='$product'/>
            <x-catalog::front.product_popular :popular="$popular" :popularAll="$popularAll" />
            <x-catalog::front.modal/>
        </div>
    </div>

</div>

@endsection
