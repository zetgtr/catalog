@extends('layouts.inner')
@section('title',$seo_title)
@section('description',$seo_description)
@section('keywords',$seo_keywords)
@section('page',true)
@section('content')
    <div class="content">
        <div class="container">
            <div class="form-groop">
                <label for="search">Поиск</label>
                <input type="text" id="search" class="form-control">
            </div>
            <div id="search_container" class="catalog__wrapper-products">
{{--                <div class="catalog__product__item">--}}
{{--                    <a href="{{ route('catalog.product',['product'=>$product->url]) }}" class="img-container">--}}
{{--                        @if ($product->images)--}}
{{--                            <img src="{{ $product->images[0] }}" alt="{{ $product->title }}">--}}
{{--                        @else--}}
{{--                            <img src="{{ asset('assets/img/no-image.png') }}" class="contain" alt="{{ $product->title }}">--}}
{{--                        @endif--}}
{{--                    </a>--}}
{{--                    <a href="{{ route('catalog.product',['product'=>$product->url]) }}" class="title-container">--}}
{{--                        <h4 class="title">{{ $product->title }}</h4>--}}
{{--                    </a>--}}
{{--                    <div class="weight">--}}
{{--                        <p>Толщина: <span>200</span> мк</p>--}}
{{--                    </div>--}}
{{--                    <div class="price">--}}
{{--                        <p><span>{{ $product->price }}</span>/пог. м</p>--}}
{{--                    </div>--}}
{{--                    <button class="button button-s popular__btn add_cart" data-product="" data-id="{{ $product->id }}">--}}
{{--                        <span>в корзину</span>--}}
{{--                    </button>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
    <input type="hidden" value="{{ $url }}" id="url">
    <x-catalog::front.templItem />
    @vite('resources/js/catalog/searchProduct.js')
@endsection
