@extends('layouts.inner')
@section('title',$seo_title)
@section('description',$seo_description)
@section('keywords',$seo_keywords)
@section('page',true)
@section('content')
    <div class="content">
        <div class="catalog__container">
            <div class="container">
                {{-- @dd($breadcrumbs) --}}
                <x-front.breadcrumbs :breadcrumbs="$breadcrumbs"/>
                <h1>{{ $title }}</h1>
                <div class="catalog @if (!empty($filter)) catalog__wrapper-filter @endif">
                    <div class="main">
                        <div class="preloader-container d-none">
                            <div class="spinner"></div>
                        </div>
                        <div class="catalog__wrapper-products-container">
                            <x-catalog::front.product :products="$catalog['products']" :path="!empty($path) ? $path : '/catalog/category/'" :url="$url" />
                            {!! $category->description !!}
                        </div>
                    </div>
                    @if(!empty($filter))
                        <x-catalog::front.filter :category="$category" />
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
