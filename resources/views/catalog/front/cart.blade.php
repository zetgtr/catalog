@extends('layouts.inner')
{{-- @section('title',$seo_title)
@section('description',$seo_description)
@section('keywords',$seo_keywords) --}}
@section('page',true)
@section('content')
<div class="content">
    <div class="cart">
        <div class="container">
            {{-- <x-front.breadcrumbs :breadcrumbs="$breadcrumbs" flag='true' /> --}}
            <h1>{{ $title }}</h1>
            <div class="cart__wrapper">
                <x-catalog::front.cart/>
            </div>
        </div>
    </div>
</div>
@endsection

