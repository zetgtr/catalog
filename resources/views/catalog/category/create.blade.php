@extends('layouts.admin')
@section('title',"Каталог")
@section('content')
    <div class="card">
        <x-admin.navigation_list.navigation :links="$links" />
        <div class="card-header">
            <h3 class="card-title">Добавить категорию</h3>

        </div>
        <x-admin.navigation_list.navigatin-js :links="$navigation" />
        <div class="card-body">
            <x-warning />
            <form action="{{ route('admin.catalog.category.store') }}" enctype="multipart/form-data" method="post" class="row row-page-create">
                @csrf
                <div class="tab-content">
                    <x-catalog::create.content />
                    <x-catalog::create.seo />
                    <x-catalog::create.photo />
                </div>
                <div>
                    <input type="submit" value="Сохранить" class="btn btn-success btn-sm">
                </div>
            </form>
        </div>
    </div>
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.catalog")}}">Каталог</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.catalog.category.index")}}">Список категорий</a></li>
            <li class="breadcrumb-item active" aria-current="page">Добавить категорию</li>
        </ol>
    </div>
@endsection
