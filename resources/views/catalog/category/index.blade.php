@extends('layouts.admin')
@section('title', 'Каталог')
@section('content')
    <div class="card">
        <x-admin.navigation_list.navigation :links="$links" />
        <div class="card-header d-flex justify-content-between flex-wrap gap-2">
            <h3 class="card-title">Список категорий</h3>
            <a href="{{ route('admin.catalog.category.create') }}" class="btn btn-primary"><i
                    class="fas fa-plus me-1"></i>Добавить категорию</a>
        </div>
        <div class="card-body">
            <x-warning />
            <div class="row row-page-create">
                <div class="col-md-8 order-md-first">
                    <div class="dd nestable" id="nestable">
                        <x-catalog::category :categories="$categories" />
                    </div>
                </div>

            </div>
        </div>
    </div>
    <input type="hidden" id="route_dd" value="{{ route('admin.catalog.category.order') }}">
    <script src="{{ asset('assets/js/admin/dnd.js') }}"></script>
    <script src="{{ asset('assets/js/admin/delete.js') }}"></script>
    <script src="{{ asset('assets/js/admin/show.js') }}"></script>
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.catalog') }}">Каталог</a></li>
            <li class="breadcrumb-item active" aria-current="page">Список категорий</li>
        </ol>
    </div>
@endsection
