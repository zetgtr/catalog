import { FrontPagination } from "./frontPagination";
class Filter {
    constructor() {
        this.range = {};
        this.popular = null;
        this.hardness = null;
        this.filterString = "";
        this.url = window.location.href.split("?")[0].split("/").pop();
        this.filteredProductsCount = 0;
        this.filteredItems = [];
        this.filteredSort = document.querySelectorAll(".ware-filter__link");
        this.messageBlock = null; // Добавляем переменную для хранения ссылки на текущее сообщение
        this.container = document.querySelector(
            ".catalog__wrapper-products-container"
        );

        this.preloader = document.querySelector(".loading-background");
        this.accordionBtns = document.querySelectorAll(".accordion-button");
        this.filterBtn = document.querySelector(".button-drawer");
        this.backdrop = document.querySelector(".sidebar");
        this.train = document.querySelector(".sidebar__train");
        this.filterCountContainerNode = document.querySelector(
            ".catalog-category__count"
        );
        this.filterCountNode = document.querySelector(
            ".catalog-category__count span"
        );
        this.checkboxes = document.querySelectorAll(
            '.ware-filter-input_ch input[type="checkbox"]'
        );
        this.sortBtn = document.querySelectorAll(".ware-filter__link");
        this.initEventListeners();
    }

    initHandlerBtn() {
        this.filterBtn.addEventListener("click", () => {
            this.openDrawer();
        });
        if (window.innerWidth < 1199) {
            this.backdrop.addEventListener("click", (e) => {
                if (e.target.id == "rail") this.closeDrawer();
            });
        }
    }
    closeDrawer() {
        this.train.classList.remove("sidebar_open");
        setTimeout(() => {
            document.body.classList.remove("sidebar-open");
            this.filterBtn.classList.remove("d-none");
            this.backdrop.classList.remove("sidebar_open");
        }, 400);
    }
    openDrawer() {
        console.log("open");
        this.filterBtn.classList.add("d-none");
        document.body.classList.add("sidebar-open");
        this.backdrop.classList.add("sidebar_open");
        setTimeout(() => {
            this.train.classList.add("sidebar_open");
        }, 0);
    }

    initEventListeners() {
        this.initSlider();

        // Обработчик для сортировки
        this.filteredSort.forEach((el) => {
            el.addEventListener("click", this.handleSortChange.bind(this));
        });

        // Обработчик для изменения чекбоксов
        this.checkboxes.forEach((checkbox) => {
            checkbox.addEventListener("change", this.handleChange.bind(this));
        });

        // Обработчик для изменения селектов (новое)
        document.querySelectorAll(".ware-filter-select").forEach((select) => {
            select.addEventListener(
                "change",
                this.handleSelectChange.bind(this)
            );
        });

        // Обработчик для аккордеона
        // $(".collapse-main").on("show.bs.collapse", (event) => {
        //     this.handleAccordionCollapse(event);
        // });
    }

    // Обработчик изменения селекта
    async handleSelectChange(e) {
        const selectNode = e.target; // Получаем измененный селект

        this.setParam(document.getElementById("filter_form")); // Обновляем параметры фильтрации на основе нового выбора

        // Асинхронно обновляем продукты, применяя новый фильтр
        await this.filterProducts();
    }

    handleAccordionCollapse(event) {
        if (event.target.classList.contains("accordion-collapse_mobile")) {
            $(".accordion-collapse_mobile.show").collapse("hide");
            return;
        }

        const collapsingElements = $(".collapsing").not(this);

        if (collapsingElements.length > 0) {
            event.preventDefault();
        } else {
            $(".collapse-main.show").collapse("hide");
        }
    }

    setParam(form) {
        document.querySelector("input[name=price_range]").disabled = true;
        const width = window.screen.width;
        const classRange = width < 992 ? "mobile" : "desktop";

        const ranges = document.querySelectorAll(
            `.accordion-item_${classRange} .filter-range`
        );

        console.log(ranges);

        const rangesData = Array.from(ranges).map((range) => {
            const [min, max] = range.value.split(";").map(Number);
            return {
                alias: range.dataset.alias,
                min: +min !== +range.dataset.min ? min : false,
                max: +max !== +range.dataset.max ? max : false,
            };
        });

        const newFormData = {};
        const newFormGetData = {};

        // Обработка чекбоксов
        document.querySelectorAll(".filter-check").forEach((el) => {
            el.disabled = true;
            if (el.checked && el.dataset.category) {
                if (!newFormData[el.dataset.category]) {
                    newFormData[el.dataset.category] = [];
                    newFormData[el.dataset.category].push(el.name);
                } else {
                    newFormGetData[el.name] = 1;
                }
            }
        });

        // Обработка select (добавлено)
        const propertyFilters = {};
        document.querySelectorAll(".ware-filter-select").forEach((select) => {
            const alias = "property_" + select.name; // Получаем alias фильтра
            const value = select.value; // Получаем выбранное значение
            if (value) {
                propertyFilters[alias] = value; // Добавляем в объект только если значение выбрано
            }
        });

        let sort = null;
        document.querySelectorAll(".ware-filter__link").forEach((item) => {
            item.querySelector("input").disabled = true;
            if (item.querySelector("input").checked) {
                sort = item.dataset.asc;
            }
        });

        const data = {
            checkbox: newFormData,
            ranges: rangesData,
            getCheckbox: newFormGetData,
            sort: sort,
            properties: propertyFilters, // Добавляем select-фильтры
        };
        console.log(data);
        window.history.replaceState(
            {},
            "",
            this.generateFilterUrl(form.dataset.category, data)
        );
    }

    generateFilterUrl(category, data) {
        let filters = [];
        this.category = category;

        // Обработка checkbox фильтров
        if (data.checkbox) {
            for (let key in data.checkbox) {
                if (data.checkbox[key].length > 0) {
                    filters.push(`${data.checkbox[key].join(",")}`);
                }
            }
        }

        const newCurrentParams = new URLSearchParams("");
        const checkboxParams = new URLSearchParams(data.getCheckbox);
        checkboxParams.forEach((value, key) => {
            newCurrentParams.set(`checkbox[${key}]`, value);
        });

        // Обработка price фильтров

        data.ranges.forEach((el) => {
            if (el.min) newCurrentParams.set(`range[${el.alias}][min]`, el.min);
            if (el.max) newCurrentParams.set(`range[${el.alias}][max]`, el.max);
        });

        // Обработка сортировки
        if (data.sort) {
            newCurrentParams.set("sort", data.sort);
        }

        // Обработка property фильтров
        if (data.properties) {
            for (let property in data.properties) {
                newCurrentParams.set(property, data.properties[property]);
            }
        }

        // Добавление фильтров в URL
        if (filters.join("/").length > 0) {
            this.filterString =
                "/filters/" +
                filters.join("/") +
                (newCurrentParams.toString().length > 0
                    ? "?" + newCurrentParams.toString()
                    : "");
        } else {
            this.filterString =
                newCurrentParams.toString().length > 0
                    ? "?" + newCurrentParams.toString()
                    : "";
        }

        console.log(
            `${window.location.origin}${
                category !== "stock" ? "/catalog/" : "/"
            }${category}${this.filterString}`
        );
        return `${window.location.origin}${
            category !== "stock" ? "/catalog/" : "/"
        }${category}${this.filterString}`;
    }

    retainParams(params, keysToRetain) {
        // Создаем новый URLSearchParams для хранения только нужных параметров
        const retainedParams = new URLSearchParams();

        // Перебираем параметры и сохраняем только те, которые нужно оставить
        params.forEach((value, key) => {
            if (keysToRetain.includes(key)) {
                retainedParams.set(key, value);
            }
        });

        return retainedParams;
    }

    mergeQueryParams(currentParams, newParams) {
        newParams.forEach((value, key) => {
            currentParams.set(key, value);
        });

        return currentParams;
    }

    async handleChange(e) {
        const checkboxNode = e.target;

        this.handleCheckboxesCheck(checkboxNode);
        this.setParam(document.getElementById("filter_form"));

        await this.filterProducts();
    }

    async handleSortChange(e) {
        const radioNode = e.target;

        this.handleRadioCheck(
            radioNode.closest(".radio").querySelector("input")
        );
        this.setParam(document.getElementById("filter_form"));

        await this.filterProducts();
    }

    updateCount() {
        if (this.filterCountNode)
            this.filterCountNode.innerText = this.filteredProductsCount;
    }

    handleCheckboxesCheck(checkboxNode) {
        this.checkboxes.forEach((checkbox) => {
            if (checkbox.id === checkboxNode.id) {
                checkbox.checked = checkboxNode.checked;
            }
        });
    }
    handleRadioCheck(radioNode) {
        this.filteredSort.forEach((checkbox) => {
            if (checkbox.querySelector("input").id === radioNode.id) {
                checkbox.querySelector("input").checked = !radioNode.checked;
            } else {
                checkbox.querySelector("input").checked = false;
            }
        });
    }

    initSlider() {
        $(".js-range-slider").ionRangeSlider({
            type: "double",
            skin: "round",
            postfix: "",
            onFinish: this.handleSliderChange.bind(this),
        });
    }

    async handleSliderChange(data) {
        const params = new URLSearchParams(window.location.search);

        this.range[data.input[0].dataset.alias] = {
            min: parseFloat(data.from),
            max: parseFloat(data.to),
        };
        this.setParam(document.getElementById("filter_form"));
        params.set(
            `range[${data.input[0].dataset.alias}]['min']`,
            parseFloat(data.from)
        );
        params.set(
            `range[${data.input[0].dataset.alias}]['max']`,
            parseFloat(data.to)
        );
        await this.filterProducts();
    }

    async filterProducts() {
        try {
            this.showPreloader();

            const data = {
                url: this.category,
            };

            const response = await axios.post(
                "/api/get_filter" + this.filterString,
                data
            );
            const viewCard = await axios.post(
                "/api/get_filter_view" + this.filterString,
                data
            );
            // const title = document.title;
            // let metaTag = document.querySelector('meta[name="description"]');
            // if (document.querySelector('.catalog-category__title'))
            //     document.querySelector('.catalog-category__title').innerText = title.data.h1
            // if (metaTag) {
            //     metaTag.setAttribute('content', title.data.description);
            // } else {
            //     metaTag = document.createElement('meta');
            //     metaTag.setAttribute('name', 'description');
            //     metaTag.setAttribute('content', title.data.description);
            //     document.head.appendChild(metaTag);
            // }
            document.querySelector("input[name=price_range]").disabled = false;
            document.querySelectorAll(".filter-check").forEach((el) => {
                el.disabled = false;
            });
            document.querySelectorAll(".ware-filter__link").forEach((item) => {
                item.querySelector("input").disabled = false;
            });
            this.filteredProductsCount = response.data.count;
            this.filteredItems = response.data.items;
            this.updateUI(viewCard.data);
            this.hidePreloader();
        } catch (error) {
            console.error("Error while filtering products:", error);
            this.hidePreloader();
        }
    }

    showPreloader() {
        this.preloader?.classList.remove("d-none");
    }

    hidePreloader() {
        this.preloader?.classList.add("d-none");
    }

    showCount() {
        this.filterCountContainerNode?.classList.remove("d-none");
    }

    updateUI(data) {
        const container = this.container;
        container.innerHTML = "";
        container.innerHTML = data;

        if (this.filteredProductsCount) {
            new FrontPagination(this.filterString);
            this.updateCount();
            this.showCount();
        } else {
            this.filterCountContainerNode?.classList.add("d-none");
        }

        const url = new URL(document.location);
        const searchParams = url.searchParams;
        searchParams.delete("page"); // удалить параметр "test"
        window.history.pushState({}, "", url.toString());
    }

    applyFilter() {
        console.log("Filter applied!");
    }
}

const filterInstance = new Filter();
window._Filter = filterInstance;
