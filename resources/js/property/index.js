import {ModalWindow} from "../../../../public/assets/js/admin/page/Data/modal/newModalWindow";

class PropertyProduct {
    constructor(){
        this.dataProperty = JSON.parse(document.getElementById('product_property').value)
        this.filtersProduct = JSON.parse(document.getElementById('filters_product').value)
        this.editBtn = document.querySelector('.btn-edit-property')
        this.saveBtn = document.querySelector('.btn-save-property')
        this.modal = new ModalWindow(document.getElementById('modaldemo8'), true)
        this.select = document.getElementById('property_select')
        this.propertyContainer = document.querySelector('.container_property')
        this.addTemplate = document.getElementById('add_property').content.children[0]
        this.addEvent()
        this.setSelectNode()
    }

    addEvent(){
        this.editBtn.addEventListener('click',this.modalOpen.bind(this))
        this.select.addEventListener('change',this.setSelectNode.bind(this))
    }

    setSelectNode(){
        if(this.dataProperty.length > 0 && +this.select.value === 0) {
            this.propertyContainer.classList.add('d-none')
        } else {
            this.propertyContainer.classList.remove('d-none')
        }
        this.propertyContainer.querySelectorAll('input').forEach(el=>{
            el.value = el.type === 'checkbox' ? 1 : ''
            el.checked = false
        })
        if (this.dataProperty.length === 0){
            this.select.classList.add('d-none')
            this.saveBtn.classList.add('d-none')
            this.setFilters(this.filtersProduct)
        }else {
            this.select.classList.remove('d-none')
            this.saveBtn.classList.remove('d-none')
            this.dataProperty.forEach(item=>{
                if(+this.select.value === +item.id) {
                    this.setFilters(item.filters)
                }
            })
        }

    }

    setFilters(filters){
        filters.forEach(filter=>{
            const input = this.propertyContainer.querySelector('input[name="filters[' + filter.alias + ']"]')
            input.value = filter.pivot.value
            input.checked = true
        })
    }

    modalOpen(){
        this.setTemplate()
        this.modal.insertTitle('Свойства')
        this.modal.insertNodeBody(this.template)
        this.modal.modal_node.querySelector('.modal-save').innerText = 'Добавить'
        this.modal.openModal()
        this.modal.close_modal = this.modalClose.bind(this)
        this.modal.save_modal = this.modalAdd.bind(this)
    }

    setDndEvent(){
        const context = this
        $(this.template).nestable({
            maxDepth: 1
        }).on('change', updateOutput);
        $('.dd').on('change', function () {
            axios.post($('#route_dd').val(),{
                items: $(this).nestable('serialize')
            }).then(({data})=>{
                context.setProperty(data.properties)
            })
        });
    }

    setProperty(properties){
        this.dataProperty = properties
        this.setTemplate()
        this.setSelect()
        this.setSelectNode()


    }

    setSelect(){
        const value = this.select.value
        this.select.innerHTML = ''
        let option = document.createElement('option')
        option.value = 0
        option.innerText = '-- Выберите --'
        option.disabled = true
        option.selected = +value === 0
        this.select.append(option)
        this.dataProperty.forEach(item=>{
            option = document.createElement('option')
            option.value = item.id
            option.innerText = item.title
            option.selected = +value === item.id
            this.select.append(option)
        })
    }

    setTemplate(){

        if(this.template)
            this.template.remove()
        this.template = document.getElementById('table_property').content.children[0].cloneNode(true)
        if (this.dataProperty.length > 0) {
            const ddList = document.createElement('ol')
            ddList.classList.add('dd-list')
            this.dataProperty.forEach(item => {
                ddList.append(this.setItem(item))
            })
            this.template.innerHTML = ''
            this.template.append(ddList)
        }
        this.modal.insertNodeBody(this.template)
        this.setDndEvent()
    }

    setItem(item){
        const ddItem = document.createElement('li')
        ddItem.classList.add('dd-item')
        ddItem.classList.add('delete-element')
        ddItem.dataset.id = item.id
        ddItem.dataset.name = item.title
        ddItem.append(this.setHandle(item.title))
        ddItem.append(this.setEdit(item))
        return ddItem
    }

    setHandle(title){
        const ddHandle = document.createElement('div')
        ddHandle.classList.add('dd-handle')
        const span = document.createElement('span')
        span.innerText = title
        ddHandle.append(span)
        return ddHandle
    }

    setEdit(item){
        const dndEdit = document.createElement('div')
        dndEdit.classList.add('dnd-edit')
        dndEdit.classList.add('show-dnd')
        dndEdit.append(this.setDeleteBtn(item))
        dndEdit.append(this.setEditBtn(item))
        return dndEdit
    }

    openEdit(item){
        const template = this.addTemplate.cloneNode(true)
        template.querySelector('input[name=title]').value = item.title
        this.modal.insertTitle('Редактировать свойство')
        this.modal.modal_node.querySelector('.modal-save').innerText = 'Сохранить'
        this.modal.insertNodeBody(template)
        this.modal.addExtendButton(this.setParamBtn())
        this.modal.save_modal = this.edit.bind(this,item.id,template)
    }

    edit(id,template){
        axios.post('property/'+ id +'/edit',new FormData(template)).then(({data})=>{
            this.setProperty(data.properties)
            this.modal.insertTitle('Свойства')
            this.modal.modal_node.querySelector('.modal-save').innerText = 'Добавить'
            this.modal.removeExtendButton('add')
            this.modal.save_modal = this.modalAdd.bind(this)
        })
    }

    setDeleteBtn(item){
        const btnDelete = document.createElement('a')
        btnDelete.classList.add('button-delete')
        btnDelete.classList.add('delete')
        btnDelete.classList.add('btn')
        btnDelete.classList.add('btn-danger')
        btnDelete.classList.add('btn-xs')
        btnDelete.classList.add('pull-right')
        btnDelete.style.color = '#FFFFFF'
        const deleteIcon = document.createElement('i')
        deleteIcon.classList.add('fa')
        deleteIcon.classList.add('fa-times')
        btnDelete.append(deleteIcon)
        btnDelete.addEventListener('click',this.deleteProperty.bind(this,item.id))
        return btnDelete
    }

    deleteProperty(id){
        axios.delete('property/'+ id +'/delete').then(({data})=>{
            this.select.value = +id === +this.select.value ? 0 : this.select.value
            this.setProperty(data.properties)
            this.modal.insertTitle('Свойства')
            this.modal.modal_node.querySelector('.modal-save').innerText = 'Добавить'
            this.modal.removeExtendButton('add')
            this.modal.save_modal = this.modalAdd.bind(this)
        })
    }

    setEditBtn(item){
        const btnEdit = document.createElement('a')
        // btnEdit.classList.add('button-edit')
        btnEdit.classList.add('btn')
        btnEdit.classList.add('btn-warning')
        btnEdit.classList.add('btn-xs')
        btnEdit.classList.add('pull-right')
        btnEdit.style.right = '5px'
        btnEdit.style.top = '4px'
        btnEdit.style.position = 'absolute'
        const editIcon = document.createElement('i')
        editIcon.classList.add('fa')
        editIcon.classList.add('fa-pencil')
        btnEdit.append(editIcon)
        btnEdit.addEventListener('click',this.openEdit.bind(this,item))
        return btnEdit
    }

    modalClose(){
        this.modal.modal_node.querySelector('.modal-save').innerText = 'Сохранить'
        this.modal.removeExtendButton('add')
    }
    modalAdd(){
        const template = this.addTemplate.cloneNode(true)
        this.modal.insertTitle('Добавить свойство')
        this.modal.modal_node.querySelector('.modal-save').innerText = 'Сохранить'
        this.modal.insertNodeBody(template)
        this.modal.addExtendButton(this.setParamBtn())
        this.modal.save_modal = this.add.bind(this,template)
    }

    add(template){
        axios.post('property/add',new FormData(template)).then(({data})=>{
            this.setProperty(data.properties)
            this.modal.insertTitle('Свойства')
            this.modal.modal_node.querySelector('.modal-save').innerText = 'Добавить'
            this.modal.removeExtendButton('add')
            this.modal.save_modal = this.modalAdd.bind(this)
        })
    }
    setParamBtn(){
        return {
            id: 'add',
            type: 'info',
            name: 'Назад',
            fn: () => {
                this.modal.insertTitle('Свойства')
                this.modal.modal_node.querySelector('.modal-save').innerText = 'Добавить'
                this.modal.insertNodeBody(this.template)
                this.modal.removeExtendButton('add')
                this.modal.save_modal = this.modalAdd.bind(this)
            }
        }
    }

}

$(document).ready(()=>{
    new PropertyProduct()
})
