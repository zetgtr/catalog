<?php

use Catalog\Enums\CatalogEnums;
use Laravel\Fortify\Features;
use Catalog\Http\Controllers\CatalogOrderController;

return [
    'swich_category' => true,
    'guard' => 'web',
    'middleware' => ['web'],
    'auth_middleware' => 'auth',
    'passwords' => 'users',
    'username' => 'email',
    'email' => 'email',
    'views' => true,
    'yml' => false,
    'company_name' => 'ИП ',
    'home' => '/home',
    'product_action' => \App\QueryBuilder\Catalog\CatalogBuilder::class,
    'prefix' => '',
    'domain' => null,
    'amount' => false,
    'limiters' => [
        'login' => null,
    ],
    'payment' => ['acquiring'],
    'redirects' => [
        'login' => null,
        'logout' => null,
        'password-confirmation' => null,
        'register' => null,
        'email-verification' => null,
        'password-reset' => null,
    ],
    'start_menu' => CatalogOrderController::class,
    'menu_links' => [
        CatalogEnums::ORDER->value => ['route' => 'admin.catalog', 'name' => 'Список заказов'],
        CatalogEnums::PRODUCT->value => ['route' => 'admin.catalog.product.index', 'name' => 'Товары'],
        CatalogEnums::CATEGORY->value => ['route' => 'admin.catalog.category.index', 'name' => 'Категории'],
//        CatalogEnums::PROMOCODE->value => ['route' => 'admin.catalog.promocode.index', 'name' => 'Промокоды'],
        CatalogEnums::FILTER->value => ['route' => 'admin.catalog.filter.index', 'name' => 'Фильтрация'],
        CatalogEnums::SETTINGS->value => ['route' => 'admin.catalog.settings.index', 'name' => 'Настройки'],
//        CatalogEnums::YML->value => ['route' => 'admin.catalog.yml.index', 'name' => 'YML'],
    ],
    'fillable' => [
        'cart' => [
            'host_name',
            'product_id',
            'count',
        ],
        'promocode' => [
            'promocode',
            'sale',
            'type',
            'promocode_category',
        ],
        'property' => [
            'title',
            'product_id',
            'order',
            'images',
            'filter_alias',
        ],
        'category' => [
            'title',
            'description',
            'url',
            'seo_title',
            'seo_description',
            'seo_keywords',
            'images',
            'parent',
            'order',
            'publish',
            'access'
        ],
        'categoryFilter' => [
            'alias',
            'name',
            'type',
            'hidden',
            'hidden_admin',
            'open'
        ],
        'filter' => [
            'alias',
            'name',
            'category_id',
        ],
        'order' => [
            'name',
            'user_id',
            'phone',
            'email',
            'url',
            'promocode',
            'promocode_sale',
            'promocode_type',
        ],
        'product' => [
            'title',
            'content',
            'characteristics',
            'url',
            'seo_title',
            'seo_description',
            'seo_keywords',
            'images',
            'access',
        ],
        'settings' => [
            'title',
            'description',
            'url',
            'paginate',
            'seo_title',
            'seo_description',
            'seo_keywords'
        ]
    ],
    'request' => [
        'property' => [
            'create' => [
                'title' => ['nullable','string'],
                'filter_alias' => ['nullable','string']
            ],
            'update' => [
                'title' => ['nullable','string'],
                'filter_alias' => ['nullable','string']
            ]
        ],
        'promocode' => [
            'create' => [
                'promocode' => ['required','string','unique:catalog_promocodes,promocode'],
                'type' => ['required','string'],
                'sale' => ['required'],
                'promocode_category' => ['nullable'],
            ],
            'update' => [
                'promocode' => ['required','string','unique:catalog_promocodes,promocode'],
                'type' => ['required','string'],
                'sale' => ['required'],
                'promocode_category' => ['nullable'],
            ],
        ],
        'category' => [
            'create' => [
                'title' => ['required'],
                'description' => ['nullable'],
                'seo_title' => ['nullable'],
                'seo_description' => ['nullable'],
                'seo_keywords' => ['nullable'],
                'images' => ['nullable'],
                'parent' => ['nullable'],
                'order' => ['nullable'],
                'access' => ['nullable']
            ],
            'update' => [
                'seo_title' => ['nullable'],
                'seo_description' => ['nullable'],
                'seo_keywords' => ['nullable'],
                'images' => ['nullable'],
                'parent' => ['nullable'],
                'order' => ['nullable'],
                'access' => ['nullable'],
                'title' => ['required'],
                'description' => ['nullable']
            ]
        ],
        'filter' => [
            'create' => [
                'name' => ['required'],
                'alias' => ['required'],
                'category_id' => ['required'],

            ],
            'update' => [
                'name' => ['required'],
                'alias' => ['required'],
                'type'=> ['required'],
                'category_id' => ['required'],
            ],
            'createCategory' => [
                'name'=>['required'],
                'alias'=>['required'],
                'type'=>['required'],
                'hidden'=>['required'],
                'hidden_admin'=>['required'],
                'open' => ['string']
            ],
            'updateCategory' => [
                'name'=>['required'],
                'alias'=>['required'],
                'type'=>['required'],
                'hidden'=>['required'],
                'hidden_admin'=>['required'],
                'open' => ['string']
            ],
        ],
        'order' => [
            'click' => [
                'name' => ['required'],
                'phone' => ['required','integer'], // проверяет каждый элемент с таблицей catalog_categories c полем id
                'email'=>['required','email'],
                'url' => ['required'],
                'product_id' => ['required','exists:catalog_products,id'],
                'price' => ['nullable'],
                'count' => ['required']
            ],
            'create' => [
                'name' => ['required'],
                'phone' => ['required','integer'], // проверяет каждый элемент с таблицей catalog_categories c полем id
                'email'=>['required','email'],
                'price' => ['nullable'],
                'user_id' => ['nullable'],
                'comment' => ['nullable'],
                'promocode' => ['nullable'],
                'promocode_sale' => ['nullable'],
                'promocode_type' => ['nullable'],
                'count' => ['required']
            ]
        ],
        'product' => [
            'create' => [
                'category_id' => ['required'],
                'category_id.*' => ['exists:catalog_categories,id'], // проверяет каждый элемент с таблицей catalog_categories c полем id
                'title'=>['required'],
                'content'=>['required'],
                'characteristics'=>['nullable'],
                'seo_title'=>['nullable'],
                'seo_description'=>['nullable'],
                'seo_keywords'=>['nullable'],
                'images'=>['nullable'],
                'access'=>['nullable'],
                'filters'=>['nullable','array'],

            ],
            'filter' => [
                'range'=>['nullable'],
                'checkbox'=>['nullable'],
                'category_id'=>['nullable'],
            ],
            'update' => [
                'category_id' => ['required'],
                'category_id.*' => ['exists:catalog_categories,id'], // проверяет каждый элемент с таблицей catalog_categories c полем id
                'title'=>['required'],
                'content'=>['required'],
                'characteristics'=>['nullable'],
                'seo_title'=>['nullable'],
                'seo_description'=>['nullable'],
                'seo_keywords'=>['nullable'],
                'images'=>['nullable'],
                'access'=>['nullable'],
                'filters'=>['nullable','array'],
            ],
            'property' => [
                'property_select' => ['required'],
                'filters'=>['nullable','array'],
            ]
        ],
        'settings' => [
            'update' => [
                'title' => ['required'],
                'description' => ['nullable'],
                'url' => ['required'],
                'paginate' => ['required','integer'],
                'seo_title' => ['nullable'],
                'seo_description' => ['nullable'],
                'seo_keywords' => ['nullable'],
            ]
        ]
    ],
    'features' => [
        Features::registration(),
        Features::resetPasswords(),
        Features::emailVerification(),
        Features::updateProfileInformation(),
        Features::updatePasswords(),
        Features::twoFactorAuthentication(),
    ],
];
