<?php

use Catalog\Http\Controllers\CartController;
use Catalog\Http\Controllers\CatalogCategoryController;
use Catalog\Http\Controllers\CatalogFilterCategoryController;
use Catalog\Http\Controllers\CatalogFilterController;
use Catalog\Http\Controllers\CatalogProductController;
use Catalog\Http\Controllers\CatalogYmlController;
use Catalog\Http\Controllers\CatalogSettingsController;
use Catalog\Http\Controllers\CatalogOrderController;
use Catalog\Http\Controllers\RouterController;
use Catalog\QueryBuilder\CatalogBuilder;
use Catalog\Http\Controllers\CatalogPromocodeController;
use Illuminate\Support\Facades\Route;


Route::middleware('web')->group(function (){
    Route::middleware('key_cart')->group(function(){
        Route::middleware('auth_pacage')->group(function () {
            Route::group(['prefix'=>"admin", 'as'=>'admin.', 'middleware' => 'is_admin'],static function(){
                Route::get("catalog", [config('catalog.start_menu'),'index'])
                    ->name("catalog")
                    ->middleware('menu.check:1001');
                Route::group(['prefix' => 'catalog', 'as' => 'catalog.'], static function() {
                    Route::resource('product', CatalogProductController::class);
                    Route::resource('category', CatalogCategoryController::class);
                    Route::resource('settings', CatalogSettingsController::class);
                    Route::resource('yml', CatalogYmlController::class);
                    Route::resource('filter', CatalogFilterController::class);
                    Route::resource('promocode', CatalogPromocodeController::class);
                    Route::resource('filter_category', CatalogFilterCategoryController::class);
                    Route::post('filter_category/order', [CatalogFilterCategoryController::class,'order'])->name('filter_category.order');
                    Route::post('filter/order', [CatalogFilterController::class,'order'])->name('filter.order');
                    Route::post('category/order', [CatalogCategoryController::class,'order'])->name('category.order');
                    Route::post('product/order', [CatalogProductController::class,'order'])->name('product.order');
                    Route::get('category/publish/{category}', [CatalogCategoryController::class,'publish'])->name('category.publish');
                    Route::delete('category/delete_image/{category}/{image}', [CatalogCategoryController::class,'deleteImageCategory'])->name('category.delete_image');
                    Route::get('product/publish/{product}', [CatalogProductController::class,'publish'])->name('product.publish');
                    Route::post('product/{product}/property/add',[CatalogProductController::class,'propertyCreate'])->name('property.add');
                    Route::post('product/{product}/property/order', [CatalogProductController::class,'propertyOrder'])->name('property.order');
                    Route::post('product/{product}/property/{property}/edit', [CatalogProductController::class,'propertyEdit'])->name('property.edit');
                    Route::delete('product/{product}/property/{property}/delete', [CatalogProductController::class,'propertyDelete'])->name('property.delete');
                    Route::put('product/{product}/property/save', [CatalogProductController::class,'propertySave'])->name('property.save');
                });
            });
        });
        try {
            $catalogBuileder = new CatalogBuilder();
            $settings = $catalogBuileder->getCatalogRouter();
            if(config('catalog.swich_category')){
                Route::get($settings['url'],[RouterController::class,"index"])->name("catalog");
            }else{
                Route::get($settings['url'],[RouterController::class,"allProduct"])->name("catalog");
            }
            Route::group(['prefix'=>$settings['url'], 'as'=>'catalog.'],static function(){
                Route::get("/{category}",[RouterController::class,'category'])->name('category');
                Route::get("{category}/filters/{filters}",[RouterController::class,'filters'])
                    ->where('filters', '.*')->name('category.filters');
                Route::get("/{category}/{product}",[RouterController::class,'product'])->name('product');

                Route::get("/{category}/{product}/{article}",[RouterController::class,'product'])->name('product.property');
            });
//            price-to-{price_to}-from-{price_from}/stones:{stones}/colors:{colors}/sort:{sort}
//            price-to-1-from-1000/stones:agat/colors:red/sort:asc
            Route::get("/cart",[RouterController::class,'cart'])->name('cart');
            Route::get("/search",[RouterController::class,'search'])->name('search');
            Route::post("/search",[RouterController::class,'searchItem']);

            Route::group(['prefix'=>"cart"],static function(){
                Route::post('/delete',[CartController::class,'delete']);
                Route::post('/add',[CartController::class,'add']);
                Route::post('/remove',[CartController::class,'remove']);
                Route::post('/delete_all',[CartController::class,'delete_all']);
                Route::post('/get',[CartController::class,'get']);
                Route::post('/get_count',[CartController::class,'get_count']);
                Route::post('/order_click',[CatalogOrderController::class,'click']);
                Route::post('/order',[CatalogOrderController::class,'store']);
            });
        } catch (Exception $exception)
        {

        }
    });
});
