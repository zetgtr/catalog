<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('catalog_filter_category', function (Blueprint $table) {
            $table->string('alias');
            $table->string('type');
            $table->boolean('hidden');
        });
        Schema::table('catalog_filters', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }

    public function down(): void
    {
        Schema::table('catalog_filter_category', function (Blueprint $table) {
            $table->dropColumn('alias');
            $table->dropColumn('type');
            $table->dropColumn('hidden');
        });
        Schema::table('catalog_filters', function (Blueprint $table) {
            $table->string('type');
        });
    }
};
