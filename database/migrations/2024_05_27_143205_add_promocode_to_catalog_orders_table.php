<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('catalog_orders', function (Blueprint $table) {
            $table->string('promocode')->nullable();
            $table->integer('promocode_sale')->nullable();
            $table->enum('promocode_type',\Catalog\Enums\PromocodeEnum::all())->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('catalog_orders', function (Blueprint $table) {
            $table->dropColumn('promocode');
            $table->dropColumn('promocode_sale');
            $table->dropColumn('promocode_type');
        });
    }
};
