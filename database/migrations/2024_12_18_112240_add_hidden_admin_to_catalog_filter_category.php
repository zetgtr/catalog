<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('catalog_filter_category', function (Blueprint $table) {
            $table->boolean('hidden_admin')->default(false);
        });
    }

    public function down()
    {
        Schema::table('catalog_filter_category', function (Blueprint $table) {
            $table->dropColumn('hidden_admin');
        });
    }
};
