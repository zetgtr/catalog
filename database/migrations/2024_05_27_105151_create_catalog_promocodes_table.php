<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('catalog_promocodes', function (Blueprint $table) {
            $table->id();
            $table->string('promocode')->unique();
            $table->integer('sale');
            $table->enum('type',\Catalog\Enums\PromocodeEnum::all());
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('catalog_promocodes');
    }
};
