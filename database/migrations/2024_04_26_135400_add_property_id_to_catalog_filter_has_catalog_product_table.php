<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('catalog_filter_has_catalog_product', function (Blueprint $table) {
            $table->foreignId('product_id')->nullable()->change();
            $table->foreignId('property_id')->after('product_id')->nullable()
                ->constrained('catalog_properties')->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::table('catalog_filter_has_catalog_product', function (Blueprint $table) {
            $table->dropColumn('property_id');
        });
    }
};
