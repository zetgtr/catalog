<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('catalog_order_has_product', function (Blueprint $table) {
            $table->foreignId('category_id')->constrained('catalog_categories');
        });
    }

    public function down(): void
    {
        Schema::table('catalog_order_has_product', function (Blueprint $table) {
            $table->dropColumn('category_id');
        });
    }
};
