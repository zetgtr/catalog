<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('catalog_product_has_properties', function (Blueprint $table) {
            $table->foreignId('product_id')
                ->references('id')->on('catalog_products')
                ->cascadeOnDelete();
            $table
                ->foreignId('property_id')
                ->references('id')->on('catalog_properties')
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('catalog_product_has_properties');
    }
};
