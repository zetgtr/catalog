<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('catalog_properties', function (Blueprint $table) {
            $table->string('filter_alias');
            $table->index('filter_alias');
        });
    }

    public function down()
    {
        Schema::table('catalog_properties', function (Blueprint $table) {
            $table->dropColumn('filter_alias');
        });
    }
};
