<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('catalog_properties', function (Blueprint $table) {
            $table->foreignId('product_id')->constrained('catalog_products')->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::table('catalog_properties', function (Blueprint $table) {
            $table->dropColumn('product_id');
        });
    }
};
