<?php

namespace Catalog\Seeders;

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \DB::table('menus')->insert($this->getData());
    }

    public function getData()
    {
        return [
            ['id'=>1001,'name'=>'Каталог','position'=>'left','logo'=>'fal fa-store','controller'=>'Catalog\Http\Controllers\CatalogOrderController','url'=>'catalog','parent'=>5,"controller_type"=>"invocable", 'order'=>3],
        ];
    }
}
