<?php

namespace Catalog\Seeders;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            CatalogSettingsSeeder::class,
            MenuSeeder::class
        ]);
    }
}