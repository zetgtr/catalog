<?php

namespace Catalog\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table = "catalog_orders";
    protected $fillable = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fillable = config('catalog.fillable.order');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class,'catalog_order_has_product','order_id','product_id')
            ->withPivot('price', 'count','category_id','property_id');
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class,'catalog_order_has_product','order_id','category_id')
            ->withPivot('price', 'count','product_id','property_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function properties()
    {
        return $this->belongsToMany(Property::class,'catalog_order_has_product','order_id','property_id')
            ->withPivot('price', 'count','product_id','category_id');
    }
}
