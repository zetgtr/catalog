<?php

namespace Catalog\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $table = "catalog_categories";
    protected $fillable = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fillable = config('catalog.fillable.category');
    }

    protected $casts = [
        'images' => 'json'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'catalog_category_has_product')->withPivot('order', 'publish')->orderBy('order');
    }

    public function children(){
        return Category::where('parent',$this->id)->where('publish',1)->orderBy('order')->get();
    }

    public function secondCategory(){
        return Category::where('parent',$this->parent)->where('publish',1)->orderBy('order')->get();
    }
    public function imagesParse(){
        return json_decode($this->images);
    }
}
