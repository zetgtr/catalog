<?php

namespace Catalog\Models;

use Catalog\Models\Filter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use HasFactory;
    protected $table = "catalog_products";

    public function getPriceAttribute()
    {
        if ($this->property->count()) {
            $minPrice = null;
            $this->property->each(function ($property) use (&$minPrice) {
                $filters = $property->filters->keyBy('alias');
                if ($filters->has('price')) {
                    $currentPrice = (int)$filters['price']->pivot->value;
                    if ($minPrice === null || $currentPrice < $minPrice) {
                        $minPrice = $currentPrice;
                    }
                }
            });

            $price = $minPrice ?? 0;
        }else{
            $filter = $this->filters->keyBy('alias');
            if(!empty($filter['price']))
                $price = (int) $filter['price']?->pivot->value;
        }

        return (!empty($price) ? $price : 0);
    }

    protected $fillable = [];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fillable = config('catalog.fillable.product');
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'catalog_category_has_product',
            'product_id', 'category_id', 'id', 'id')->withPivot('publish');
    }
    public function img()
    {
        if(!empty($this->images) && $this->images)
            return json_decode($this->images);
        return false;
    }

    public function filters(): BelongsToMany
    {
        return $this->belongsToMany(Filter::class, 'catalog_filter_has_catalog_product',
            'product_id', 'filter_id',  'id', 'id')->withPivot('value')
            ->withPivot('category_id')
            ->leftJoin('catalog_filter_category', 'catalog_filters.category_id', '=', 'catalog_filter_category.id')
            ->orderBy('catalog_filter_category.order');
    }

    public function property(){
        return $this->hasMany(Property::class,'product_id','id')->with('filters.categoryFilter')->orderBy('order')->orderByDesc('id');
    }
}
