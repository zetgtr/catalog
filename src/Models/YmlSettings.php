<?php

namespace Catalog\Models;

use Illuminate\Database\Eloquent\Model;

class YmlSettings extends Model
{
    protected $table = 'yml_settings';
    protected $fillable = ['date'];
}
