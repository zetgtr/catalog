<?php

namespace Catalog\Models;

use Catalog\Models\CategoryFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class Filter extends Model
{
    use HasFactory;
    protected $table = "catalog_filters";
    protected $fillable = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fillable = config('catalog.fillable.filter');
    }
    public function categoryFilter()
    {
        return $this->belongsTo(CategoryFilter::class,'category_id','id');
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'catalog_filter_has_catalog_product',
             'filter_id', 'product_id','id', 'id');
    }

    private function paretCategoryFilter($id,&$query){
        $categoryParent = Category::where('parent',$id)->get();
        foreach ($categoryParent as $category){
            $query->orWhere('category_id', $category->id);
            $this->paretCategoryFilter($category->id,$query);
        }
    }

    public function categoryFiltersValue($category){
        $query = DB::table('catalog_filter_has_catalog_product')
            ->where('filter_id', $this->id);

        if ($category) {
            $query->where('category_id', $category->id);
            $this->paretCategoryFilter($category->id,$query);
        }


        if (Schema::hasColumn('catalog_products', 'amount')){
            $query->leftJoin('catalog_products','catalog_products.id','=','catalog_filter_has_catalog_product.product_id')
                ->where('catalog_products.amount','>',0);
        }

        return $query->selectRaw('MIN(CAST(value AS SIGNED)) as min, MAX(CAST(value AS SIGNED)) as max')->first();
     }

}
