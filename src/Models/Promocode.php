<?php

namespace Catalog\Models;

use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{
    public $timestamps = false;

    protected $table = 'catalog_promocodes';

    protected $fillable = [
        'promocode',
        'sale',
        'type',
    ];
}
