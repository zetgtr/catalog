<?php

namespace Catalog\Models;

use Catalog\Models\Filter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class CategoryFilter extends Model
{
    use HasFactory;
    protected $table = "catalog_filter_category";
    protected $fillable = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fillable = config('catalog.fillable.categoryFilter');
    }

    public function filter()
    {
        return $this->hasMany(Filter::class,
         'category_id', 'id');
    }
}
