<?php

namespace Catalog\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Property extends Model
{
    use HasFactory;
    protected $table = "catalog_properties";
    protected $fillable = [];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fillable = config('catalog.fillable.property');
    }

    public function product()
    {
        return $this->hasOne(Product::class,'id','product_id');
    }

    public function filters(): BelongsToMany
    {
        return $this->belongsToMany(Filter::class, 'catalog_filter_has_catalog_product',
            'property_id', 'filter_id',  'id', 'id')->withPivot('value')
            ->withPivot('category_id')
            ->leftJoin('catalog_filter_category', 'catalog_filters.category_id', '=', 'catalog_filter_category.id')
            ->orderBy('catalog_filter_category.order');
    }

    public function filter(): BelongsTo
    {
        return $this->belongsTo(Filter::class, 'alias');
    }
}
