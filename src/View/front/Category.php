<?php

namespace Catalog\View\front;

use Catalog\QueryBuilder\CartBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Category extends Component
{
    public function __construct($categories = null)
    {
        $this->categories = $categories ?? \Catalog\Models\Category::get();
    }

    public function render(): View
    {
        return view('catalog::components.front.category ',[
            'categories' => $this->categories
        ]);
    }
}
