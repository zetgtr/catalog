<?php

namespace Catalog\View\front;

use Catalog\Models\Product;
use Catalog\QueryBuilder\CartBuilder;
use Catalog\QueryBuilder\CatalogBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\View\Component;

class Cartitem extends Component
{
    private ?int $count;

    public function __construct(CartBuilder $builder)
    {
        $this->count =  $builder->getCount();

    }

    public function render(): View
    {
        return view('catalog::components.front.cartitem',[
            'count' =>  $this->count,

        ]);
    }
}
