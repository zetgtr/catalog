<?php

namespace Catalog\View\front;

use Catalog\Models\Product;
use Catalog\Models\Settings;
use Catalog\QueryBuilder\CatalogBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\View\Component;

class CategoryFilter extends Component
{
    private array $categories;
    public function __construct(CatalogBuilder $catalogBuilder,Request $request)
    {
        $url = $request->segment(count($request->segments()));
        $this->categories = $catalogBuilder->getCategoryUrl($url,$request);
        $settings = Settings::first();

        if($settings && $settings->nesting) {
            $children = $this->categories['secondCategory']->children();
            if($children->count()) {
                $this->categories['categories'] = $this->categories['secondCategory']->children();
            }else{
                $this->categories['categories'] = $this->categories['secondCategory']->secondCategory();
            }
        }
    }

    public function render(): View
    {
        return view('catalog::components.front.category_filter',[
            'categories'=>$this->categories['categories'],
        ]);
    }
}
