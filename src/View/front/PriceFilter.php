<?php

namespace Catalog\View\front;

use Catalog\Models\Product;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class PriceFilter extends Component
{
    private int $priceMin;
    private int $priceMax;
    private int $priceMinRequest;
    private int $priceMaxRequest;
    public function __construct()
    {
        $this->priceMin = Product::min('price');
        $this->priceMax = Product::max('price');
        if(request('price'))
            list($this->priceMinRequest, $this->priceMaxRequest) = [request('price')['min'],request('price')['max']];
        else
            list($this->priceMinRequest,$this->priceMaxRequest) = [$this->priceMin, $this->priceMax];
    }

    public function render(): View
    {
        return view('catalog::components.front.price_filter',[
            'priceMin'=>$this->priceMin,
            'priceMax'=>$this->priceMax,
            'priceMinRequest'=>$this->priceMinRequest,
            'priceMaxRequest'=>$this->priceMaxRequest
        ]);
    }
}
