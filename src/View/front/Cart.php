<?php

namespace Catalog\View\front;

use Catalog\QueryBuilder\CartBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Cart extends Component
{
    private ?array $products;
    private ?int $total;
    public function __construct(CartBuilder $builder)
    {
        $this->products = $builder->getAll();
        $this->total = $builder->getTotalPrice();
    }

    public function render(): View
    {
        return view('catalog::components.front.cart',[
            'products' => $this->products,
            'total' => $this->total
        ]);
    }
}
