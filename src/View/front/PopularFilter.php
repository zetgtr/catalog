<?php

namespace Catalog\View\front;

use Catalog\Models\Product;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class PopularFilter extends Component
{
    public function render(): View
    {
        return view('catalog::components.front.popular_filter',[
            'filters' => config('catalog.filters')
        ]);
    }
}
