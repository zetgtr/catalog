<?php

namespace Catalog\View\promocode;

use Catalog\QueryBuilder\CatalogBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class PromocodeList extends Component
{
    public function __construct(CatalogBuilder $catalogBuilder)
    {
        $this->promocodes = $catalogBuilder->getPromocodes();
    }

    public function render(): View
    {
        return view('catalog::components.promocode.list',['promocodes' => $this->promocodes]);
    }
}
