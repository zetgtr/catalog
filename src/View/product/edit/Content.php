<?php

namespace Catalog\View\product\edit;

use Catalog\Models\CategoryFilter;
use Catalog\QueryBuilder\CatalogBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Content extends Component
{
    public function __construct(CatalogBuilder $catalogBuilder, $product)
    {
        $this->categories = $catalogBuilder->getCategoryAll();
        $this->product = $product;
    }

    public function render(): View
    {
        return view('catalog::components.product.edit.content',[
            'categories' => $this->categories,
            'product' => $this->product
        ]);
    }
}
