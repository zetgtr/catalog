<?php

namespace Catalog\View\product\edit;

use Catalog\Models\CategoryFilter;
use Catalog\QueryBuilder\CatalogBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Property extends Component
{
    public function __construct(CatalogBuilder $catalogBuilder, $product)
    {
        $this->filters = CategoryFilter::query()->with('filter',function ($q){
            $q->orderBy('name');
        })->where('hidden_admin', false)
            ->orderByRaw("CASE WHEN type = 'slider-range' THEN 1 WHEN type = 'text' THEN 2 ELSE 3 END")
            ->get();

        $this->product = $product;
    }

    public function render(): View
    {
        return view('catalog::components.product.edit.property',[
            'filters' => $this->filters,
            'product' => $this->product
        ]);
    }
}
