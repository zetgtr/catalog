<?php

namespace Catalog\View\product;

use Catalog\QueryBuilder\CatalogBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Index extends Component
{
    public function __construct(CatalogBuilder $catalogBuilder)
    {
        $this->categories = $catalogBuilder->getCategory();
    }

    public function render(): View
    {
        return view('catalog::components.product.index',['categories'=>$this->categories]);
    }
}
