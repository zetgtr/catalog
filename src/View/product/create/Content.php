<?php

namespace Catalog\View\product\create;

use Catalog\Models\CategoryFilter;
use Catalog\QueryBuilder\CatalogBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Content extends Component
{
    public function __construct(CatalogBuilder $catalogBuilder)
    {
        $this->categories = $catalogBuilder->getCategoryAll();
        $this->filters = CategoryFilter::orderBy('order')->get();
    }

    public function render(): View
    {
        return view('catalog::components.product.create.content',[
            'categories' => $this->categories,
            'filters' => $this->filters
            ]);
    }
}
