<?php

namespace Catalog\View\create;

use Catalog\QueryBuilder\CatalogBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Content extends Component
{
    public function __construct(CatalogBuilder $catalogBuilder)
    {
        $this->categories = $catalogBuilder->getCategoryAll();
    }

    public function render(): View
    {
        return view('catalog::components.create.content', ['categories'=>$this->categories]);
    }
}
