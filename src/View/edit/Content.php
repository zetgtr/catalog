<?php

namespace Catalog\View\edit;

use Catalog\QueryBuilder\CatalogBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Content extends Component
{
    public function __construct(CatalogBuilder $catalogBuilder,$category)
    {
        $this->categories = $catalogBuilder->getCategoryAll();
        $this->category = $category;
    }

    public function render(): View
    {
        return view('catalog::components.edit.content', ['categories'=>$this->categories,'category'=>$this->category]);
    }
}
