<?php

namespace Catalog\View;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use Catalog\Models\Settings as Model;

class Settings extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct()
    {
        $this->settings = Model::query();
        $this->settings = $this->settings->find(1);
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('catalog::components.settings',['settings'=>$this->settings]);
    }
}
