<?php

namespace Catalog\View\filter;

use Catalog\Models\CategoryFilter;
use Catalog\Models\Product;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class FrontFilter extends Component
{
    public function __construct($category)
    {
        $this->category = $category;
        $this->filters = CategoryFilter::orderBy('order')->get();

        $properties = \Catalog\Models\Property::query()
            ->select('filter_alias', 'title') // Выбираем нужные поля
            ->get()
            ->groupBy('filter_alias') // Группируем по filter_alias на уровне PHP
            ->map(function ($group) {
                return $group->pluck('title'); // Извлекаем только значения title
            })
            ->toArray(); // Преобразуем в массив

        $this->properties = $properties;
    }

    public function render(): View
    {
        return view('catalog::components.front.filters',[
            'categories_filters' => $this->filters,
            'category' => $this->category,
            'properties' => $this->properties,
        ]);
    }
}
