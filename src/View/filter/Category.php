<?php

namespace Catalog\View\filter;

use Catalog\Models\CategoryFilter;
use Catalog\Models\Product;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Category extends Component
{
    public function render(): View
    {
        return view('catalog::components.filter.category',[
            'categories' => CategoryFilter::orderBy('order')->get()
        ]);
    }
}
