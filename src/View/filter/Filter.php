<?php

namespace Catalog\View\filter;

use Catalog\Models\CategoryFilter;
use Catalog\Models\Product;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Filter extends Component
{
    public function __construct($category)
    {
        $this->filters = $category->filter()->orderBy('order')->get();
        $this->category = $category;
    }

    public function render(): View
    {
        return view('catalog::components.filter.filter',[
            'filters'=>$this->filters,
            'category'=>$this->category,
            'categories' => CategoryFilter::orderBy('order')->get()
        ]);
    }
}
