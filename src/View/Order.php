<?php

namespace Catalog\View;

use Catalog\QueryBuilder\CatalogBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Order extends Component
{
    public function __construct(CatalogBuilder $catalogBuilder)
    {
        $this->orders = $catalogBuilder->getOrder();

    }

    public function render(): View
    {
        return view('catalog::components.order',['orders' => $this->orders]);
    }
}
