<?php

namespace Catalog\Providers;

use Catalog\Http\Middleware\SetCookieMiddleware;
use Catalog\QueryBuilder\CatalogBuilder;
use Catalog\QueryBuilder\QueryBuilder;
use Catalog\View\filter\FrontFilter;
use Catalog\View\front\Filters;
use Catalog\View\Order;
use Catalog\View\product\edit\Property;
use Catalog\View\promocode\PromocodeList;
use Catalog\View\Settings;
use Catalog\View\edit\Content as EditContent;
use Catalog\View\product\create\Content as ProductCreateContent;
use Catalog\View\product\edit\Content as ProductEditContent;
use Catalog\View\product\Index;
use Catalog\View\create\Content as CreateContent;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\AliasLoader;
use Catalog\Http\Middleware\Auth as PacageAuth;
use Catalog\QueryBuilder\CartBuilder;
use Catalog\View\filter\Category;
use Catalog\View\filter\Filter;
use Catalog\View\front\Cart;
use Catalog\View\front\Cartitem;
use Catalog\View\front\CategoryFilter;
use Catalog\View\front\PopularFilter;
use Catalog\View\front\PriceFilter;
use Illuminate\Http\Request;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class CatalogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        }

        $this->app['router']->aliasMiddleware('key_cart', SetCookieMiddleware::class);
        $this->app['router']->aliasMiddleware('auth_pacage', PacageAuth::class);

        $this->publishes([
            __DIR__ . '/../../database/migrations' => database_path('migrations'),
            __DIR__ . '/../../resources/scss' => resource_path('sass/components'),
        ], 'migrations');

        $this->publishes([
            __DIR__.'/../../config/catalog.php' => config_path('catalog.php'),
        ], 'catalog_config');

        $this->publishes([
            __DIR__.'/../../resources/views' => resource_path('views/vendor/catalog'),
        ], 'catalog_views');

        $this->publishes([
            __DIR__.'/../../resources/js' => resource_path('js/catalog'),
        ], 'catalog_script');

        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'catalog');
        $this->loadViewsFrom(__DIR__.'/../../resources/components', 'catalog');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');
        $this->components();
    }

    private function components()
    {
        Blade::component(CreateContent::class, 'catalog::create.content');
        Blade::component(EditContent::class, 'catalog::edit.content');
        Blade::component(\Catalog\View\front\Category::class, 'catalog::front.category');
        Blade::component(ProductCreateContent::class, 'catalog::product.create.content');
        Blade::component(ProductEditContent::class, 'catalog::product.edit.content');
        Blade::component(Property::class, 'catalog::product.edit.property');
        Blade::component(Index::class, 'catalog::product.index');
        Blade::component(Order::class, 'catalog::order');
        Blade::component(Settings::class, 'catalog::settings');
        Blade::component(PriceFilter::class, 'catalog::front.price_filter');
        Blade::component(PopularFilter::class, 'catalog::front.popular_filter');
        Blade::component(CategoryFilter::class, 'catalog::front.category_filter');
        Blade::component(Cart::class, 'catalog::front.cart');
        Blade::component(Cartitem::class, 'catalog::front.cartitem');
        Blade::component(Category::class, 'catalog::filter.category');
        Blade::component(Filter::class, 'catalog::filter.filter');
        Blade::component(FrontFilter::class, 'catalog::front.filters');
        Blade::component(PromocodeList::class, 'catalog::promocode.list');
    }

    private function singletons()
    {
        $this->app->singleton(CreateContent::class, function ($app) {
            $builder = $app->make(CatalogBuilder::class);
            return new CreateContent($builder);
        });
        $this->app->singleton(EditContent::class, function ($app, $parameters) {
            $builder = $app->make(CatalogBuilder::class);
            $category = $parameters['category'];
            return new EditContent($builder, $category);
        });
        $this->app->singleton(ProductCreateContent::class, function ($app) {
            $builder = $app->make(CatalogBuilder::class);
            return new ProductCreateContent($builder);
        });
        $this->app->singleton(ProductEditContent::class, function ($app, $parameters) {
            $builder = $app->make(CatalogBuilder::class);
            $product = $parameters['product'];
            return new ProductEditContent($builder,$product);
        });
        $this->app->singleton(Index::class, function ($app) {
            $builder = $app->make(CatalogBuilder::class);
            return new Index($builder);
        });
        $this->app->singleton(Order::class, function ($app) {
            $builder = $app->make(CatalogBuilder::class);
            return new Order($builder);
        });
        $this->app->singleton(Settings::class, function ($app) {
            return new Settings();
        });
        $this->app->singleton(PriceFilter::class, function ($app) {
            return new PriceFilter();
        });
        $this->app->singleton(PopularFilter::class, function ($app) {
            return new PopularFilter();
        });
        $this->app->singleton(Cart::class, function ($app) {
            $builder = $app->make(CartBuilder::class);
            return new Cart($builder);
        });
        $this->app->singleton(Cartitem::class, function ($app) {
            $builder = $app->make(CartBuilder::class);
            return new Cartitem($builder);
        });
        $this->app->singleton(FrontFilter::class, function ($app, $parameters) {
            $category = $parameters['category'];
            return new FrontFilter($builder);
        });
        $this->app->singleton(CategoryFilter::class, function ($app) {
            $builder = $app->make(CatalogBuilder::class);
            $request = $app->make(Request::class);
            return new CategoryFilter($builder,$request);
        });
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(QueryBuilder::class, CatalogBuilder::class);
        $this->mergeConfigFrom(__DIR__.'/../../config/catalog.php', 'catalog');
        $this->singletons();
    }
}
