<?php

namespace Catalog\Enums;

enum PromocodeEnum: string
{
    case PRICE = 'price';
    case PROCENT = 'procent';

    public static function all(){
        return [
            self::PRICE->value,
            self::PROCENT->value,
        ];
    }

    public static function name($type){
        return match ($type) {
            self::PRICE->value => 'Цена',
            self::PROCENT->value => 'Процент',
        };
    }
    public static function type($type){
        return match ($type) {
            self::PRICE->value => 'руб. ',
            self::PROCENT->value => '%',
        };
    }
}
