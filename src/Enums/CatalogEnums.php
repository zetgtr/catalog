<?php

namespace Catalog\Enums;

enum CatalogEnums: string
{
    case ORDER = "order";
    case PRODUCT = "product";
    case CATEGORY = "categories";
    case FILTER = "filter";
    case SETTINGS = "settings";
    case PROMOCODE = "promocode";
    case YML = "yml";

    case CONTENT = "content";
    case PROPERTY = "property";
    case SEO = "seo";
    case PHOTO = "photo";
    public static function all(): array
    {
        return [
            self::ORDER->value,
            self::PRODUCT->value,
            self::CATEGORY->value,
            self::FILTER->value,
            self::SETTINGS->value,
            self::PROMOCODE->value,
            self::YML->value,
        ];
    }
}
