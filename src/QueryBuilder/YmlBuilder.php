<?php

namespace Catalog\QueryBuilder;

use Carbon\Carbon;
use Catalog\Models\Category;
use Catalog\Models\Product;
use Catalog\Models\Settings;
use Catalog\Models\YmlSettings;

class YmlBuilder
{
    public function __construct()
    {
        $this->name = env('APP_NAME');
        $this->url = env('APP_URL');
        $this->compony_name = config('catalog.company_name');
        $this->categories = Category::where('publish',true)->get();
        $this->products = Product::whereHas('categories',function ($q){
            $q->where('catalog_category_has_product.publish',true);
        })->get();
        $this->settings = Settings::query()->first();
    }

    public function set(){
        $this->setHeader();
        $this->setCategories();
        $this->setProducts();
        $this->setEnd();
    }

    private function setHeader(){
        $this->out = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
        $this->out .= '<yml_catalog date="' . date('Y-m-d H:i') . '">' . PHP_EOL;
        $this->out .= '<shop>' . PHP_EOL;
        $this->out .= '<name>'.$this->name.'</name>' . PHP_EOL;
        $this->out .= '<name>'.$this->compony_name.'</name>' . PHP_EOL;
        $this->out .= '<name>'.env("APP_URL").'</name>' . PHP_EOL;
        $this->out .= '<currencies>' . PHP_EOL;
        $this->out .= '<currency id="RUR" rate="1"/>' . PHP_EOL;
        $this->out .= '</currencies>' . PHP_EOL;
    }

    private function setCategories(){
        $this->out .= '<categories>' . PHP_EOL;
        foreach ($this->categories as $category){
            if (!$category->parent) {
                $this->out .= '<category id="' . $category->id . '">' . $category->title . '</category>' . PHP_EOL;
            } else {
                $this->out .= '<category id="' . $category->id . '" parentId="' . $category->parent . '">' . $category->title . '</category>' . PHP_EOL;
            }
        }
        $this->out .= '</categories>' . PHP_EOL;
    }

    private function setProducts(){
        $this->out .= '<offers>' . PHP_EOL;
        foreach ($this->products as $product){
            if($product){
                $this->out .= '<offer id="' . $product->id . '">' . PHP_EOL;
                $this->out .= '<url>'.$this->url.'/'.$this->settings->url.'/product/'. $product->url . '/</url>' . PHP_EOL;
                $this->out .= '<price>'. $product->price . '</price>' . PHP_EOL;
                $this->out .= '<currencyId>RUR</currencyId>' . PHP_EOL;
                $this->out .= '<categoryId>' . $product->categories->first()->id . '</categoryId>' . PHP_EOL;
                if($product->img())
                    $this->out .= '<picture>'.$this->url.$product->img()[0].'</picture>' . PHP_EOL;
                $this->out .= '<name>'.$product->title.'</name>' . PHP_EOL;
                $this->out .= '<description><![CDATA[' .  $product->content . ']]></description>' . PHP_EOL;
                $this->out .= '</offer>' . PHP_EOL;
            }

        }

        $this->out .= '</offers>' . PHP_EOL;
    }


    private function setEnd(){
        $this->out .= '</shop>' . PHP_EOL;
        $this->out .= '</yml_catalog>' . PHP_EOL;
        $file = file_put_contents('products.yml',$this->out);
        YmlSettings::query()->updateOrCreate(['id'=>1],['date' => Carbon::now()]);
    }

}
