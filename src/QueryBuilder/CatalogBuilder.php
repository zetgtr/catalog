<?php

namespace Catalog\QueryBuilder;

use Catalog\Models\Filter;
use Catalog\Enums\CatalogEnums;
use Catalog\Models\Category;
use Catalog\Models\Order;
use Catalog\Models\CategoryFilter;
use Catalog\Models\Product;
use Catalog\Models\Promocode;
use Catalog\Models\Settings;
use Catalog\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

class CatalogBuilder extends FilterBuilder
{

    public function __construct()
    {
        $this->order = Order::query();
        $this->product = Product::query();
        $this->category = Category::query();
        $this->settings = Settings::query();
        $settings = $this->settings->first();
        $this->paginate = $settings ? $settings->paginate : 10;
        $this->bredcrumbs = [['title'=>'Главная','url'=>'/'],['title'=>$settings?$settings->title:"Каталог",'url'=>"/".($settings?$settings->url:"catalog")]];
        $this->setFilters();
    }

    public function setFilters(){
        $orderBuilderClass = config('catalog.order_builder');
        if($orderBuilderClass) {
            $builder = new $orderBuilderClass;
            $builder->setFilters($this->order);
        }
    }

    public function getNavigationLinks($key = null)
    {
        $links = config('catalog.menu_links');
        if($key)
            $links[$key]['active'] = true;
        return $links;
    }
    public function getNavigationLinksJs($key = null)
    {
        $links = [CatalogEnums::FILTER->value => ['url' => CatalogEnums::FILTER->value, 'name' => 'Фильтры'],
            CatalogEnums::CATEGORY->value => ['url' => CatalogEnums::CATEGORY->value, 'name' => 'Категории'],
        ];
        if($key)
            $links[$key]['active'] = true;
        return $links;
    }

    public function search(string $text)
    {
        $products = $this->product->where('title', 'like', '%'.$text.'%')->get();
        foreach ($products as $key=>$product)
        {
            $products[$key]->url = route("admin.catalog.product.edit",$product->id);
            $products[$key]->deleteUrl = route("admin.catalog.product.destroy",$product->id);
        }

        return $products;
    }

    public function getNavigationPageLink($key)
    {

        $links = [CatalogEnums::CONTENT->value => ['url' => CatalogEnums::CONTENT->value, 'name' => 'Контент']];

        $links[CatalogEnums::PROPERTY->value] = ['url' => CatalogEnums::PROPERTY->value, 'name' => 'Характеристики'];
        $links[CatalogEnums::PHOTO->value] = ['url'=> CatalogEnums::PHOTO->value, 'name' => 'Фото'];
        $links[CatalogEnums::SEO->value] = ['url' => CatalogEnums::SEO->value, 'name' => 'SEO'];
        $path = request()->path();
        if (strpos($path, 'admin/catalog/category/') !== false) {
            unset($links[CatalogEnums::PROPERTY->value]);
        }

        $links[$key]['active'] = true;
        return $links;
    }

    public function getOrder()
    {
        return $this->order->orderBy('created_at','desc')->paginate(15);
    }
    public function getProduct()
    {
        return $this->product->get();
    }

    private function setCategoryParent($items)
    {
        foreach ($items as $item)
        {
            $this->model = Category::query();
            $parent = $this->model->where('parent','=',$item->id)->orderBy('order')->get();
            if (count($parent) > 0) {
                $item->parent = $parent;
                $this->setCategoryParent($item->parent);
            }
        }

        return $items;
    }

    public function getCategoryParent()
    {
        $category = $this->category->where('parent','=',null)->orderBy('order')->paginate($this->paginate);
        return $this->setCategoryParent($category);
    }
    public function setProductParent(Collection $items)
    {


        foreach ($items as $item)
        {
            $this->model = Product::query();
            $parent = $this->model->where('parent','=',$item->id)->orderBy('order')->get();
            if (count($parent) > 0) {
                $item->parent = $parent;
                $this->setProductParent($item->parent);
            }
        }

        return $items;
    }

    public function getProductParent(): Collection
    {
        $product = $this->category->where('parent','=',null)->orderBy('order')->get();
        return $this->setProductParent($product);
    }
    public function getCategory()
    {
        return $this->getCategoryParent();
    }

    public function setOrderCategory(array $items, int $parent = null)
    {
        foreach ($items as $key=>$item)
        {
            $this->category->where('id',$item['id'])->update(['parent'=>$parent,'order'=>$key]);
            $this->category = Category::query();
            if(!empty($item['children']))
            {
                $this->setOrderCategory($item['children'], $item['id']);
            }
        }
    }

    public function setOrderProduct(array $items, int $category)
    {
        $category = Category::find($category);
        foreach ($items as $key=>$item)
        {
            $product = Product::find($item['id']);
            $category->products()->updateExistingPivot($product->id, ['order' => $key]);
            $category->save();
        }

    }

    public function getProductCategory(string $categoryId)
    {
        $category = Category::find($categoryId);
        return [
            'products'=>$category->products()->get(),
            'category'=>$this->category->where('parent',$category->id)->get()
        ];
    }

    public function getCategoryAll()
    {
        return $this->category->orderBy('order')->get();
    }

    public function getProductBreadcrumb(string $categoryId)
    {
        $category = Category::find($categoryId);
        $breadcrumbs = [];

        // Добавляем текущую категорию в хлебные крошки
        $breadcrumbs[] = [
            'title' => $category->title,
            'url' => route('admin.catalog.product.show', $category->id)
        ];

        if ($category->parent) {
            $parentBreadcrumbs = $this->getProductBreadcrumb($category->parent);
            $breadcrumbs = array_merge($parentBreadcrumbs, $breadcrumbs);
        }

        return $breadcrumbs;

    }

    public function getProductFrontBreadcrumb(string $url = null,$category = null)
    {
        $product =  $this->product->where('url',$url)->with(['categories'])->first();
        $category = $category ?? $product->categories->first();
        $breadcrumbs = $this->bredcrumbs;



        // Если у текущей категории есть родительская категория, добавляем ее в хлебные крошки
        if ($category->parent) {
            $parentBreadcrumbs = $this->getSiteBreadcrumb($category->parent,false);
            $breadcrumbs = array_merge($breadcrumbs,$parentBreadcrumbs);

        }
        $breadcrumbs[] = [
            'title' => $category->title,
            'url' => route('catalog.category', $category->url)
        ];

        $breadcrumbs[] = [
            'title' => $product->title,
        ];

        return $breadcrumbs;
    }

    public function getSiteBreadcrumb(string $categoryId = null,$breadcrumbs = true)
    {
        $category = Category::find($categoryId);
        $breadcrumbs = $breadcrumbs ? $this->bredcrumbs : [];

        // Добавляем текущую категорию в хлебные крошки
        if($category)
        {

            if ($category->parent) {
                $parentBreadcrumbs = $this->getSiteBreadcrumb($category->parent,false);
                $breadcrumbs = array_merge($breadcrumbs,$parentBreadcrumbs);
            }
            $breadcrumbs[] = [
                'title' => $category->title,
                'url' => route('catalog.category', $category->url)
            ];
        }

        return $breadcrumbs;
    }
    private function setCatalogRouter(Collection $items){
        foreach ($items as $key=>$item)
        {

            $this->model = Category::query();
            $parent = $this->model->where('parent', '=', $item->id)->orderBy('order')->get();
            if (count($parent) > 0) {
                $item->parent = $parent;
                $this->setCatalogRouter($item->parent);
            }
            $item->title = "Catalog";

            $products = $item->products()->get();
            if(count($products)>0) {
                foreach ($products as $product)
                    $product->title = "Product";
                $item->parent = $products;
            }

        }
        return $items;
    }

    public function getCatalogRouter()
    {
        return $this->settings->find(1);
    }

    private function decodeImage($items)
    {
        foreach ($items as $key=>$item)
        {
            $items[$key]->images = $item->images;
        }
        return $items;
    }

    public function getCategoryUrl(string $url,$request){
        if($url !== "catalog") {
            $category = $this->category->where("url", $url)->first();
            $request->category_id = $category->id;
            $this->setFilter($request);
            $this->category = Category::query();
            $products = $this->getAll();

            return [
                "categories"=>$this->decodeImage($this->getCategoryParent()),
                "products"=>$this->decodeImage($products),
                "count"=>$this->getCount()['count'],
                "secondCategory" => $category,
                ];
        }else{
            return [
                "categories"=>$this->decodeImage($this->getCategoryParent()),
                "products"=>null,
            ];
        }
    }

    public function getProductUrl(string $url)
    {
        $product = $this->product->where('url',$url)->whereHas('categories',function (Builder $q){
            $q->whereRaw('catalog_category_has_product.publish = 1');
        })->firstOrFail();
        $product->images = json_decode($product->images);
        return $product;
    }
    public function getProductPopular(Product $product)
    {
        return $this->decodeImage($product->categories->first()->products->except($product->id));
    }

    public function getProductPopularAll(Product $product)
    {
        $data = collect();
        foreach (Category::get() as $item){
            $data->push([
                'category'=>$item,
                'products'=>$this->decodeImage($item->products->except($product->id))
            ]);
        }
        return $data;
    }

    public function searchProduct(string $searchTerm){
            $categoryStal = CategoryFilter::where('name', request('filter_category'))->first();

            $products = Product::where(function ($query) use ($searchTerm) {
                $query->where('title', 'LIKE', '%' . $searchTerm . '%')->orWhere(function ($q) use ($searchTerm){
                    $q->whereHas('filters.categoryFilter',function($q) use ($searchTerm){
                        $q->where('alias','article')->where('catalog_filter_has_catalog_product.value','like', '%'.$searchTerm.'%');
                    });
                });
            });
            $categorySearch = Category::where(function ($query) use ($searchTerm) {
                $query->where('title', 'LIKE', '%' . $searchTerm . '%');
            })->first();

            if($categoryStal){
                $products->with(['filters' => function ($query) use ($categoryStal) {
                    $query->where('catalog_filters.category_id', $categoryStal->id);
                }]);
            }
            $categorySearchProducts = [];
            if(!empty($categorySearch->products)){
                foreach ($categorySearch->products as $item){
                    $item->category = $categorySearch->id;
                    $item->url = route('catalog.product', ['category' => $categorySearch->url, 'product' => $item->url]);
                    $item->searchPrice = intval($item->price);
                    $categorySearchProducts[] = $item;
                }
            }

            
            $products = $products->get();
            if($products->count())
                foreach($products as $key=>$product){
                    $category = $product->categories()->first();
                    if($category) {
                        $product->category = $category->id;
                        $product->url = route('catalog.product', ['category' => $category->url, 'product' => $product->url]);
                        $product->searchPrice = intval($product->price);
                    }
                }
            $productsArray = $products->toArray();
            $allProducts = array_merge($categorySearchProducts, $productsArray);
            return collect($allProducts);
    }

    public function setFilterProduct($filters, $product)
    {
        if(!$product->property->count()) {
            $product->filters()->detach();
            if ($filters)
                foreach ($filters as $key => $value) {
                    $filter = Filter::where('alias', $key)->first();
                    if ($filter && ($filter->type === 'checkbox' && $value) || $filter->type !== 'checkbox') {
                        foreach ($product->categories as $category)
                            $product->filters()->attach($filter, ['value' => $value, 'category_id' => $category->id]);
                    }
                }
        }
    }
    public function setFilterProperty($filters, $property)
    {
       $property->filters()->detach();
       if ($filters)
           foreach ($filters as $key => $value) {
               $filter = Filter::where('alias', $key)->first();
               if ($filter && ($filter->type === 'checkbox' && $value) || $filter->type !== 'checkbox') {
                   foreach ($property->product->categories as $category)
                       $property->filters()->attach($filter, ['value' => $value, 'category_id' => $category->id]);
               }
           }
    }

    public function getPromocodes(){
        return Promocode::get();
    }
}
