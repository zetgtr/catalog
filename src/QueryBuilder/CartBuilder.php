<?php

namespace Catalog\QueryBuilder;

use App\Jobs\GetBitrix;
use App\Jobs\GetBitrixProductJob;
use Catalog\Enums\PromocodeEnum;
use Catalog\Models\Cart;
use Catalog\Models\Category;
use Catalog\Models\Product;
use Catalog\Models\Property;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class CartBuilder
{
    private $cart;
    private ?string $hostName;

    public function __construct(Request $request)
    {
        $this->hostName = Cookie::get('key_cart');
        $this->cart = Cart::where('host_name',$this->hostName);
    }

    public function clear()
    {
        $this->cart->delete();
    }

    public function add($request)
    {
        $cart = $this->cart
            ->where('product_id',$request->input('id'))
            ->where('property_id',$request->input('property_id'))
            ->where('category_id',$request->input('category_id'))->first();
        return $this->setCart($cart,$request->input('id'),$request->input('count'),$request->input('property_id'),$request->input('category_id'));
    }

    private function setCart($cart, $product_id,$count,$property_id,$category_id)
    {
        $product = Product::find($product_id);
        $propertyData = Property::find($property_id);

        if ($count > get_product_filters(!empty($propertyData) ? $propertyData : $product, 'amount'))
            return false;

        if(!$cart) $cart = new Cart();
        $cart->host_name = $this->hostName;
        $cart->product_id = $product_id;
        $cart->property_id = $property_id;
        $cart->category_id = $category_id;
        $cart->count = $count;
        if($cart->count > 0)
            $cart->save();
        else
            $cart->delete();
        return true;
    }


    public function delete($request)
    {
        $cart = $this->cart->where('product_id',$request->input('id'))->first();
        if($cart)
            $cart->delete();
    }

    public function remove($request)
    {
        // Находим запись в корзине
        $cart = $this->cart
            ->where('product_id', $request->input('id'))
            ->where('property_id', $request->input('property_id') ?? null)
            ->where('category_id', $request->input('category_id'))
            ->first();

        // Если запись найдена
        if ($cart) {
            // Уменьшаем количество на 1
            $cart->count = max(0, $cart->count - 1); // Предотвращаем отрицательные значения

            // Если количество стало 0, можно удалить запись (опционально)
            if ($cart->count === 0) {
                $cart->delete();
            } else {
                $cart->save(); // Сохраняем изменения
            }
        } else {
            // Опционально: обработка случая, если запись не найдена
            throw new \Exception('Запись в корзине не найдена');
        }
    }


    public function getCount()
    {
        $count = 0;
        foreach(Cart::where('host_name',$this->hostName)->get() as $cart)
        {
            $count += $cart->count;
        }
        return $count;
    }

    public function getTotalPrice($promocode = null){
        $products = $this->getAll();
        $price = 0;
        foreach($products as $product)
        {
            if($product->propertyData)
                $price += get_product_filters($product->propertyData,'price') * $product->count;
            else
                $price += $product->price*$product->count;
        }

        if($promocode){
            return match ($promocode->type){
                PromocodeEnum::PRICE->value => $price - $promocode->sale,
                PromocodeEnum::PROCENT->value => $price - ($price / 100 * $promocode->sale),
            };
        }

        return $price;
    }

    public function getProductCount($id)
    {
        $cart = Cart::where('host_name',$this->hostName)->where('product_id',$id)->first();
        if($cart)
            return $cart->count;

        return 0;
    }

    public function getAll()
    {
        $products = [];
        foreach(Cart::where('host_name',$this->hostName)->get() as $cart)
        {
            $product = $cart->product()->first();
            $product->count = $cart->count;
            $product->propertyData = Property::find($cart->property_id);
            $product->property_id = $cart->property_id;
            $product->category = Category::find($cart->category_id);
            $product->category_id = $cart->category_id;
            $products[] = $product;
        }
        return $products;
    }
}
