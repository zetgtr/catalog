<?php

namespace Catalog\QueryBuilder;

use Catalog\Models\Category;
use Catalog\Models\CategoryFilter;
use Catalog\Models\Filter;
use Catalog\Models\Product;
use Catalog\Models\Settings;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

class FilterBuilder
{
    public function getPaginate()
    {
        return Settings::first()->paginate;
    }

    private function paretCategoryFilter($id,&$query){
        $categoryParent = Category::where('parent',$id)->get();
        foreach ($categoryParent as $category){
            $query->orWhere('id', $category->id);
            $this->paretCategoryFilter($category->id,$query);
        }
    }


    public function setProperty($where){
        $this->model->where(function (Builder $query) use ($where) {
            $query->where(function (Builder $query) use ($where)  {
                    $query->doesntHave('property');
                    $query->whereHas('filters', $where);
                });
            $query->orWhere(function (Builder $query) use ($where) {
                $query->whereHas('property', function ($query) use ($where) {
                    $query->whereHas('filters', $where);
                });
            });
        });
    }
    public function setCheckbox($checkboxSegment){
        $values = array_flip($checkboxSegment);
        return array_fill_keys(array_keys($values), 1);
    }
    public function setFilter($request)
    {
        $parameters = $request->all();
        $propertyFilters = array_filter($parameters, function($key) {
            return strpos($key, 'property_') === 0;
        }, ARRAY_FILTER_USE_KEY);

        if (!empty($propertyFilters)) {
            $this->model = Product::query()
                ->with('filters')
                ->with('property.filters')
                ->whereHas('categories', function ($query) use ($request) {
                    if (!empty($request->category_id)) {
                        $query->where('id', $request->category_id);
                        $this->paretCategoryFilter($request->category_id, $query);
                    }
                    $query->where('catalog_category_has_product.publish', 1);
                })
                ->whereHas('property', function ($query) use ($propertyFilters) {
                    foreach ($propertyFilters as $key => $value) {
                        $alias = substr($key, 9);
                        $valueArray = is_array($value) ? $value : [$value];
                        $query->where('catalog_properties.filter_alias', $alias)
                            ->whereIn('catalog_properties.title', $valueArray);
                    }
                });
//            dd($this->model->get());
        } else {
            $this->model = Product::query()
                ->with('filters')
                ->with('property.filters')
                ->whereHas('categories', function ($query) use ($request) {
                    if (!empty($request->category_id)) {
                        $query->where('id', $request->category_id);
                        $this->paretCategoryFilter($request->category_id, $query);
                    }
                    $query->where('catalog_category_has_product.publish', 1);
                });
        }

        $this->request = $request;

        $filtersCollect = Filter::with('categoryFilter')
            ->whereIn('alias', [
                ...(!empty($this->request->range) ? array_keys($this->request->range) : []),
                ...(!empty($this->request->checkbox) ? array_keys($this->request->checkbox) : []),
                ...(!empty($this->request->property) ? array_keys($this->request->property) : [])
            ])
            ->get()
            ->keyBy('alias');
        if (!empty($this->request->range)) {
            foreach ($this->request->range as $alias => $range) {
                $filter = $filtersCollect[$alias];
                if ($filter) {
                    $this->setProperty(function ($query) use ($filter, $range) {
                        $query->where('filter_id', $filter->id)
                            ->whereBetween('value', [
                                (int) (!empty($range['min']) ? $range['min'] : 0),
                                (int) (!empty($range['max']) ? $range['max'] : $filter->categoryFiltersValue(false)->max)
                            ]);
                    });
                }
            }
        }

        $filters = [];

        try {
            foreach ($this->request->checkbox as $alias => $checkbox) {
                if (!empty($filtersCollect[$alias])) {
                    $filter = $filtersCollect[$alias];
                    if (empty($filters[$filter->categoryFilter->id])) {
                        $filters[$filter->categoryFilter->id] = [];
                    }
                    if ($filter) {
                        $filterCondition = function ($query) use ($filter) {
                            $query->where('filter_id', $filter->id);
                        };
                        $filters[$filter->categoryFilter->id][] = $filterCondition;
                    }
                }
            }
        } catch (\Exception $exception) {
            dd($this->request->all());
        }

        if (!empty($filters)) {
            foreach ($filters as $key => $category) {
                $this->setProperty(function (Builder $query) use ($filters, $category) {
                    $query->where(function (Builder $query) use ($filters, $category) {
                        foreach ($category as $filterCondition) {
                            $query->orWhere($filterCondition);
                        }
                    });
                });
            }
        }

        // Сортировка по цене
        if ($request->sort) {
            $sort = $request->sort;
            $filter = Filter::where('alias', 'price')->first();
            if ($filter) {
                $sortedProductIds = \DB::table('catalog_filter_has_catalog_product')
                    ->select('product_id')
                    ->where('filter_id', $filter->id)
                    ->orderByRaw('CAST(value AS SIGNED) ' . $sort)
                    ->pluck('product_id')
                    ->filter()
                    ->toArray();
                if (!empty($sortedProductIds)) {
                    $this->model->whereIn('catalog_products.id', $sortedProductIds)
                        ->orderByRaw('FIELD(catalog_products.id, ' . implode(',', $sortedProductIds) . ')');
                }
            }
        }

        // Поиск по названию
        if (isset($request->search)) {
            $this->model->where('title', 'LIKE', '%' . $request->search . '%');
        }

        // Сортировка по названию
        if ($request->sort_title) {
            $asc = $request->asc;
            $this->model->orderBy('title', $asc ? 'asc' : 'desc');
        }

        $this->model->distinct();
    }


    public function getCount()
    {
        return [
            'count'=>$this->model->count(),
            'paginate'=> $this->getPaginate()
        ];
    }

    public function getFilterView($request)
    {
        $settings = Settings::first();

        $products = $this->model->distinct()->paginate($this->getPaginate());

        foreach ($products as $product)
            $product->images = $product->images ? json_decode($product->images) : null;
        return \view('catalog::components.front.product',['products'=>$products,
        'category' => Category::find($request->category_id),
        'path'=> "/". ($request->category_id ? $settings->url.'/category/' : ""),
        'count'=>$this->model->count()]);
    }

    public function getAll()
    {
        $product = new Product();
        if (request('amount') && request('amount') == "1" && Schema::hasColumn($product->getTable(), 'amount')) {
            return $this->model->distinct('catalog_products.id')->where('amount', '>', 0)->paginate($this->getPaginate());
        } else {
            return $this->model->distinct('catalog_products.id')->paginate($this->getPaginate());
        }
    }
}
