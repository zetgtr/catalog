<?php

namespace Catalog\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;

class SetCookieMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $cookieName = 'key_cart';
        if (!Cookie::get($cookieName)) {
            $cookieValue = Str::random(20);
            $cookieMinutes = 30 * 30 * 30 * 30;
            Cookie::queue($cookieName, $cookieValue, $cookieMinutes);
        }
        return $next($request);
    }
}
