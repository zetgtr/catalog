<?php

namespace Catalog\Http\Controllers;

use App\Models\Admin\Catalog\Filter;
use Catalog\Enums\CatalogEnums;
use App\Http\Controllers\Controller;
use Catalog\Models\Property;
use Catalog\Requests\Product\CreatePropertyRequest;
use Catalog\Requests\Product\CreateRequest;
use Catalog\Models\Category;
use Catalog\Models\Product;
use Catalog\QueryBuilder\CatalogBuilder;
use Catalog\QueryBuilder\FilterBuilder;
use Catalog\Requests\Product\FilterRequest;
use Catalog\Requests\Product\PropertySaveRequest;
use Catalog\Requests\Product\UpdateProductRequest;
use Catalog\Requests\Product\UpdatePropertyRequest;
use Illuminate\Http\Request;

class CatalogProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(CatalogBuilder $catalogBuilder)
    {
        return view('catalog::catalog.product.index',['links' => $catalogBuilder->getNavigationLinks(CatalogEnums::PRODUCT->value)]);
    }

    public function search(string $text, CatalogBuilder $catalogBuilder)
    {
        return $catalogBuilder->search($text);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(CatalogBuilder $catalogBuilder)
    {
        return view('catalog::catalog.product.create',[
            'links' => $catalogBuilder->getNavigationLinks(CatalogEnums::PRODUCT->value),
            'navigation' => $catalogBuilder->getNavigationPageLink(CatalogEnums::CONTENT->value)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateRequest $request,CatalogBuilder $catalogBuilder)
    {
        $product = Product::create($request->validated());
        if ($product) {
            if (!empty(config('catalog.builder.product.name'))){
                $name = config('catalog.builder.product.name');
                $method = config('catalog.builder.product.method');
                $builder = new $name();
                $builder->$method($request,$product);
            }
            $product->categories()->sync($request->getCategoriesIds());
            $catalogBuilder->setFilterProduct($request->filters,$product);
            return \redirect()->route('admin.catalog.product.edit',$product)->with('success', __('messages.admin.catalog.product.store.success'));
        }
        return \redirect()->route('admin.catalog.product.create')->with('error', __('messages.admin.catalog.product.store.fail'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id,CatalogBuilder $catalogBuilder)
    {
        $data = $catalogBuilder->getProductCategory($id);

        return view('catalog::catalog.product.show',[
            'links' => $catalogBuilder->getNavigationLinks(CatalogEnums::PRODUCT->value),
            'categories'=>$data['category'],
            'category' => $id,
            'products' => $data['products'],
            'breadcrumb' => $catalogBuilder->getProductBreadcrumb($id)
            ]);
    }

    public function filter(FilterBuilder $builder,FilterRequest $request)
    {
        $builder->setFilter($request);
        return $builder->getCount();
    }

    public function filterView(FilterBuilder $builder,FilterRequest $request)
    {
        $builder->setFilter($request);
        return $builder->getFilterView($request);
    }

    public function order(Request $request, CatalogBuilder $catalogBuilder){
        $catalogBuilder->setOrderProduct($request->all()['items'],$request->all()['category']);
    }

    public function publish(Product $product,Request $request)
    {
        $category = Category::find($request->get('category'));
        $category->products()->updateExistingPivot($product->id, ['publish' => ! $category->products()->where('id', $product->id)->first()->pivot->publish]);
        if ($category->save()) return ['status' => true, 'publish' =>  $category->products()->where('id', $product->id)->first()->pivot->publish];
        else  return ['status' => false];
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product,CatalogBuilder $catalogBuilder)
    {
        return view('catalog::catalog.product.edit',[
            'links' => $catalogBuilder->getNavigationLinks(CatalogEnums::PRODUCT->value),
            'product'=>$product,
            'navigation' => $catalogBuilder->getNavigationPageLink(CatalogEnums::CONTENT->value)
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProductRequest $request, Product $product,CatalogBuilder $catalogBuilder)
    {
        $product = $product->fill($request->validated());
        if ($product->save()) {
            if (!empty(config('catalog.builder.product.name'))){
                $name = config('catalog.builder.product.name');
                $method = config('catalog.builder.product.method');
                $builder = new $name();
                $builder->$method($request,$product);
            }
            $product->categories()->sync($request->getCategoriesIds());

            $catalogBuilder->setFilterProduct($request->filters,$product);
            return \redirect()->route('admin.catalog.product.edit',['product'=>$product])->with('success', __('messages.admin.catalog.product.update.success'));
        }

        return \back()->with('error', __('messages.admin.catalog.product.update.fail'));
    }

    public function propertyCreate(CreatePropertyRequest $request,Product $product){
        $product->property()->create($request->validated());
        return ['status'=>true,'properties'=>$product->property];
    }
    public function propertyEdit(UpdatePropertyRequest $request,Product $product,Property $property){
        $property->update($request->validated());
        return ['status'=>true,'properties'=>$product->property];
    }
    public function propertyDelete(Request $request,Product $product,Property $property){
        $property->delete();
        return ['status'=>true,'properties'=>$product->property];
    }

    public function propertyOrder(Request $request,Product $product){
        foreach ($request->all()['items'] as $order=>$item){
            Property::where('id',$item['id'])->update(['order'=>$order + 1]);
        }
        return ['status'=>true,'properties'=>$product->property];
    }

    public function propertySave(PropertySaveRequest $request,Product $product,CatalogBuilder $catalogBuilder){
        $property = Property::find($request->property_select);
        if($property){
            $catalogBuilder->setFilterProperty($request->filters,$property);
        }
        return redirect()->back()->with('success','Успешно сохранено')
            ->with('property',$property)->with('navigation',$catalogBuilder->getNavigationPageLink(CatalogEnums::PROPERTY->value));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        try {
            $product->delete();
            $response = ['status' => true,'message' => __('messages.admin.catalog.product.destroy.success')];
        } catch (Exception $exception)
        {
            $response = ['status' => false,'message' => __('messages.admin.catalog.product.destroy.fail').$exception->getMessage()];
        }

        return $response;
    }
}
