<?php

namespace Catalog\Http\Controllers;

use App\Http\Controllers\Controller;
use Catalog\Enums\CatalogEnums;
use Catalog\Models\Promocode;
use Catalog\QueryBuilder\CatalogBuilder;
use Catalog\Requests\Order\CreateRequest;
use Catalog\Requests\Promocode\CreatePromocodeRequest;
use Catalog\Requests\Promocode\UpdatePromocodeRequest;

class CatalogPromocodeController extends Controller
{
    public function index(CatalogBuilder $catalogBuilder)
    {
        return view('catalog::catalog.promocode.index',['links' => $catalogBuilder->getNavigationLinks(CatalogEnums::PROMOCODE->value)]);
    }
    public function store(CreatePromocodeRequest $request)
    {
        Promocode::create($request->validated());
        return redirect()->back()->with('success','Успешно добавлено');
    }
    public function edit(Promocode $promocode)
    {
        return $promocode;
    }
    public function update(Promocode $promocode,UpdatePromocodeRequest $request)
    {
        $promocode->update($request->validated());
        return redirect()->back()->with('success','Успешно обновлено');
    }
    public function destroy(Promocode $promocode)
    {
        $promocode->delete();
        return ['status'=>true];
    }
}
