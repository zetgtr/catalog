<?php

namespace Catalog\Http\Controllers;

use App\Http\Controllers\Controller;

use Catalog\Models\Category;
use Catalog\QueryBuilder\CartBuilder;
use Catalog\QueryBuilder\CatalogBuilder;
use Catalog\Requests\Product\FilterRequest;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function add(Request $request, CartBuilder $builder)
    {
        if($builder->add($request)){
            $response = new Response([
                'status'=>true,
                'count'=>$builder->getCount(),
                'total'=>$builder->getTotalPrice(),
                'product_count'=>$builder->getProductCount($request->input('id'))
            ]);
        }else{
            $response = new Response(['status'=>false,'message'=>'Нет товаров в данном количестве','count'=>$builder->getCount(),'product_count'=>$builder->getProductCount($request->input('id'))]);
        }
        return $response;
    }

    public function delete(Request $request, CartBuilder $builder)
    {
        $builder->delete($request);
        $response = new Response(['status'=>true,'count'=>$builder->getCount(),'total'=>$builder->getTotalPrice()]);
        return $response;
    }

    public function remove(Request $request, CartBuilder $builder)
    {
        $builder->remove($request);
        $response = new Response(['status'=>true,'count'=>$builder->getCount(),'total'=>$builder->getTotalPrice(),'product_count'=>$builder->getProductCount($request->input('id'))]);
        return $response;
    }

    public function delete_all()
    {

    }

    public function get_count(CartBuilder $builder)
    {
        return ['status'=>true,'count'=>$builder->getCount()];
    }
    public function get(CartBuilder $builder)
    {
        return $builder->getAll();
    }
}

