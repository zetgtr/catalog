<?php

namespace Catalog\Http\Controllers;

use App\Http\Controllers\Controller;

use App\QueryBuilder\TitleBuilder;
use Catalog\Models\Category;
use Catalog\Models\Product;
use Catalog\Models\Property;
use Catalog\QueryBuilder\CatalogBuilder;
use Catalog\Requests\Product\FilterRequest;
use Catalog\QueryBuilder\FilterBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class RouterController extends Controller
{
    function index(CatalogBuilder $catalogBuilder,Request $request)
    {

        $settings = $catalogBuilder->getCatalogRouter();

        return \view('catalog::catalog.front.category',[
            'seo_title' => $settings->seo_title ?? $settings->title,
            'title' => $settings->title,
            'seo_description' => $settings->seo_description,
            'seo_keywords' => $settings->seo_keywords,
            'breadcrumbs' => $catalogBuilder->getSiteBreadcrumb(),
            'catalog' => $catalogBuilder->getCategoryUrl('catalog',$request)
        ]);
    }

    public function allProduct(CatalogBuilder $catalogBuilder,FilterRequest $request )
    {
        $builder = new FilterBuilder();
        $builder->setFilter($request);
        $settings = $catalogBuilder->getCatalogRouter();
        $catalog = ['products'=>$builder->getAll()];
        return \view('catalog::catalog.front.category_product',[
            'seo_title' => $settings->seo_title,
            'title' => $settings->title,
            'seo_description' => $settings->seo_description,
            'seo_keywords' => $settings->seo_keywords,
            'breadcrumbs' => $catalogBuilder->getSiteBreadcrumb(),
            'catalog' => $catalog,
            'category' => null,
            'url' => "",
            'path'=>$settings->url,
            'filter' => true,
        ]);
    }


    function category(CatalogBuilder $catalogBuilder,string $url,FilterRequest $request,TitleBuilder $titleBuilder)
    {
        $settings = $catalogBuilder->getCatalogRouter();
        $category = Category::query()->where('url',$url)->where('publish',true)->firstOrFail();
        $catalog = $catalogBuilder->getCategoryUrl($url,$request);
        $titleBuilder->setFilters($request->checkbox);
        return \view('catalog::catalog.front.category_product',[
            'seo_title' => $titleBuilder->setTitle($category),
            'title' => $titleBuilder->setH1($category),
            'seo_description' => $titleBuilder->setDescription($category),
            'seo_keywords' => $catalog['secondCategory']->seo_keywords,
            'breadcrumbs' => $catalogBuilder->getSiteBreadcrumb($category->id),
            'catalog' => $catalog,
            'category' => $category,
            'url' => $url,
            'filter' => true,
        ]);
    }

    function filters(CatalogBuilder $catalogBuilder,string $url,string $filters,FilterRequest $request,TitleBuilder $titleBuilder)
    {
        $filterBuilder = new FilterBuilder();
        $settings = $catalogBuilder->getCatalogRouter();
        $category = Category::query()->where('url',$url)->where('publish',true)->firstOrFail();
        $catalog = $catalogBuilder->getCategoryUrl($url,$request);

        $titleBuilder->setFilters($request->checkbox);
        return \view('catalog::catalog.front.category_product',[
            'seo_title' => $titleBuilder->setTitle($category),
            'title' => $titleBuilder->setH1($category),
            'seo_description' => $titleBuilder->setDescription($category),
            'seo_keywords' => $catalog['secondCategory']->seo_keywords,
            'breadcrumbs' => $catalogBuilder->getSiteBreadcrumb($category->id),
            'catalog' => $catalog,
            'category' => $category,
            'url' => $url,
            'filter' => true,
        ]);
    }

    function product(CatalogBuilder $catalogBuilder,string $categoryUrl,string $productUrl,string|null $article = null)
    {
        $settings = $catalogBuilder->getCatalogRouter();
        $product = $catalogBuilder->getProductUrl($productUrl);
        $category = Category::where('url',$categoryUrl)->where('publish',true)->firstOrFail();
        View::share('category',$category);
        $property = $product->property->filter(function (Property $property) use ($article){
            return $property->filters()
                ->where('catalog_filters.alias','article')
                ->where('catalog_filter_has_catalog_product.value',$article)
                ->count();
        })?->first();
        return view('catalog::catalog.front.product',[
            'title' => $product->title,
            'url'=> $settings['url'],
            'seo_title' => $product->seo_title,
            'seo_description' => $product->seo_description,
            'seo_keywords' => $product->seo_keywords,
            'breadcrumbs' => $catalogBuilder->getProductFrontBreadcrumb($productUrl,$category),
            'product' => $catalogBuilder->getProductUrl($productUrl),
            'popular' => $catalogBuilder->getProductPopular($product),
            'property' => $property,
            'popularAll' => $catalogBuilder->getProductPopularAll($product)
        ]);
    }
    function search(CatalogBuilder $catalogBuilder)
    {
        $settings = $catalogBuilder->getCatalogRouter();
        return view('catalog::catalog.front.search',[
            'title' => 'Поиск товаров',
            'url'=> $settings['url'],
            'seo_title' => 'Поиск товаров',
            'seo_description' => 'Поиск товаров',
            'seo_keywords' => 'Поиск товаров'
        ]);
    }

    function searchItem(CatalogBuilder $catalogBuilder,Request $request){
        $products = $catalogBuilder->searchProduct($request->input('search'));
        foreach ($products as $product){
            try {
               $product->price;
           }catch (\Exception $exception){
           }
       }
        if($products){
            return ['status'=>true,'products'=>$products];
        }

        return ['status'=>false,'Поиск не дал результатов'];
    }

    function cart(CatalogBuilder $catalogBuilder)
    {
        $settings = $catalogBuilder->getCatalogRouter();
        return view('catalog::catalog.front.cart',[
            'title' => "Корзина",
            'seo_title' => 'Корзина',
            'seo_description' => 'Корзина',
            'breadcrumbs' => $catalogBuilder->getSiteBreadcrumb(),
            'seo_keywords' => 'Корзина'
        ]);
    }
}
