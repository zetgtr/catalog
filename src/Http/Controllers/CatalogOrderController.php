<?php

namespace Catalog\Http\Controllers;

use Acquiring\QueryBuilder\PaymentBuilder;
use App\Models\Admin\Settings\Settings;
use App\Notifications\OrderFormNotification;
use Catalog\Enums\CatalogEnums;
use App\Http\Controllers\Controller;
use Catalog\Enums\PromocodeEnum;
use Catalog\Models\Order;
use Catalog\QueryBuilder\CartBuilder;
use Catalog\QueryBuilder\CatalogBuilder;
use Catalog\Requests\Order\ClickRequest;
use Catalog\Requests\Order\CreateRequest;
use Illuminate\Support\Facades\Notification;

class CatalogOrderController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function index(CatalogBuilder $catalogBuilder)
    {
        return view('catalog::catalog.order.index',['links' => $catalogBuilder->getNavigationLinks(CatalogEnums::ORDER->value)]);
    }

    public function click(ClickRequest $request)
    {
        $order = new Order();
        $order->name = $request->name;
        $order->phone = $request->phone;
        $order->user_id = \Auth::user()?->id;
        $order->email = $request->email;
        $order->url = $request->url;
        if($order->save()){
            $order->products()->attach($request->product_id, [
                'price' => $request->price,
                'count' => $request->count,
            ]);
            return ['status'=>true,'message'=>'Товар успешно оформлен'];
        }
        return ['status'=>false,'message'=>'Не удалось оформить заказ'];
    }

    public function store(CreateRequest $request,CartBuilder $catalogBuilder)
    {
        $cart = $catalogBuilder->getAll();
        if(config('catalog.amount'))
            foreach ($cart as $product){
                if(get_product_filters($product->propertyData ?? $product, 'amount') < $product->count)
                    return redirect()->back();
            }
        $order = Order::create($request->validated());
        $productsText =  "";
        $price = 0;
        if($order){
            foreach($cart as $product) {
                $priceProduct = 0;
                if($product->propertyData)
                    $priceProduct = get_product_filters($product->propertyData,'price');
                else
                    $priceProduct = $product->price;

                $order->products()->attach($product->id, [
                    'price' => $priceProduct,
                    'count' => $product->count,
                    'category_id' => $product->category->id,
                    'property_id' => $product->property_id,
                ]);

                $price += $priceProduct * $product->count;
                $productsText .= $product->title." - ".$product->count.' шт. / '. number_format($priceProduct, 0, '.', ' ')  .' руб. ('. number_format(($priceProduct*$product->count), 0, '.', ' ').' руб.) <br>';
            }

            if($order->promocode){
                $price = match ($order->promocode_type){
                    PromocodeEnum::PRICE->value => $price - $order->promocode_sale,
                    PromocodeEnum::PROCENT->value => $price - ($price / 100 * $order->promocode_sale),
                    default => $price
                };
            }
            $email = false;
            $settings = Settings::first();
            $data = $request->all();
            $data['order_id'] = $order->id;
            if($order->promocode) {
                $data['promocode'] = $order->promocode;
                $data['promocode_sale'] = $order->promocode_sale . ' ' . PromocodeEnum::type($order->promocode_type);
            }
            if($settings)
                $email = $settings->site_email;
            if($email)
                Notification::route('mail', $email)->notify(new OrderFormNotification($data,$productsText,number_format($price, 0, '.', ' ') ));

            Notification::route('mail', $order->email)->notify(new OrderFormNotification($data,$productsText,number_format($price, 0, '.', ' ') ));

            $orderBuilderClass = config('catalog.order_builder');
            if($orderBuilderClass) {
                $builder = new $orderBuilderClass;
                $builder->init($order);
            }

            $catalogBuilder->clear();

            if(in_array($request->payment, config('catalog.payment'))){
                $payment = new PaymentBuilder();
                return redirect($payment->getUrl($order));
            }

            if($request->action === 'form')
                return redirect('thanks?order='.$order->id);

            return ['status'=>true,'message'=>'Заказ успешно оформлен'];
        }
        return ['status'=>false,'message'=>'Не удалось оформить заказ'];
    }
}
