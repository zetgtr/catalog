<?php

namespace Catalog\Http\Controllers;

use Catalog\Enums\CatalogEnums;
use App\Http\Controllers\Controller;
use Catalog\Models\Filter;
use Catalog\Models\CategoryFilter;
use Catalog\Requests\Filter\UpdateRequest;
use Catalog\Requests\Filter\CreateRequest;
use Catalog\Models\Settings;
use Catalog\QueryBuilder\CatalogBuilder;
use Illuminate\Http\Request;

class CatalogFilterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(CatalogBuilder $catalogBuilder)
    {
        return view("catalog::catalog.filter.index",[
            'links'=>$catalogBuilder->getNavigationLinks(CatalogEnums::FILTER->value),
            'linksJs'=> $catalogBuilder->getNavigationLinksJs(CatalogEnums::FILTER->value),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateRequest $request)
    {
        $filter = Filter::create($request->validated());
        if ($filter) {
            return back()->with('success','Успешно добавлено');
        }
        return back()->with('error', 'Не удалось добавить');
    }
    public function order(Request $request)
    {
        foreach ($request->all()['items'] as $key=>$item)
        {
            $store = Filter::find($item['id']);
            $store->order = $key;
            $store->save();
        }
    }
    /**
     * Display the specified resource.
     */
    public function show(string $id,CatalogBuilder $catalogBuilder)
    {
        return view('catalog::catalog.filter.filter',['category'=>CategoryFilter::find($id),
            'links'=>$catalogBuilder->getNavigationLinks(CatalogEnums::FILTER->value)]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id)
    {
        return [
            "status" => true,
            "data" =>Filter::find($id)
        ];
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, int $id)
    {
        $settings = Filter::query()->find($id);
        $product = $settings->fill($request->validated());
        if ($product->save()) {
            return \back()->with('success','Успешно отредактирован');
        }

        return \back()->with('error','Ошибка редактирования');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $category = Filter::query()->find($id);
        $category->delete();
        return ['status'=>true];
    }
}
