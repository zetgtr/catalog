<?php

namespace Catalog\Http\Controllers;

use Catalog\Enums\CatalogEnums;
use App\Http\Controllers\Controller;
use Catalog\Models\CategoryFilter;
use Catalog\Requests\Filter\CreateCategoryRequest;
use Catalog\Requests\Filter\UpdateCategoryRequest;
use Catalog\Requests\Settings\UpdateRequest;
use Catalog\Models\Settings;
use Catalog\QueryBuilder\CatalogBuilder;
use Illuminate\Http\Request;
class CatalogFilterCategoryController extends Controller
{

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateCategoryRequest $request)
    {
        if(CategoryFilter::create($request->validated()))
            return back()->with('succsess','Категория успешно добавлена');
        return back()->with('error','Ошибка добавления категории');
    }

    public function order(Request $request)
    {
        foreach ($request->all()['items'] as $key=>$item)
        {
            $store = CategoryFilter::find($item['id']);
            $store->order = $key;
            $store->save();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(CategoryFilter $categoryFilter)
    {
        return $categoryFilter;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id)
    {
        return [
            "status" => true,
            "data" =>CategoryFilter::find($id)
        ];
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCategoryRequest $request, int $id)
    {
        $settings = CategoryFilter::query()->find($id);
        $product = $settings->fill($request->validated());
        if ($product->save()) {
            return back()->with('success', 'Категория успешно обновлена');
        }

        return \back()->with('error', "Не удалось обновить категорию");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $category = CategoryFilter::query()->find($id);
        $category->delete();
        return ['status'=>true];
    }
}
