<?php

namespace Catalog\Requests\Category;

use Catalog\Models\Category;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function all($keys = null)
    {
        $data = parent::all($keys);

        $images = $data['images'];

        $jsonImages = json_encode($images);

        $data['images'] = $jsonImages;

        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'url' => ['required', Rule::unique(Category::class)->ignore($this->id)],
            ...config('catalog.request.category.create')
        ];
    }

    public function prepareForValidation()
    {
        if($this->input('parent') == "on")
        {
            $this->merge([
                'parent' => null
            ]);
        }
        if (!$this->input('url')) {
            $this->merge([
                'url' => str_slug_new($this->input('title'))
            ]);
        }
        if (!$this->input('seo_title')) {
            $this->merge([
                'seo_title' => $this->input('title')
            ]);
        }
        $images = [];
        if ($this->file('img')) {
            foreach ($this->file('img') as $image)
            {
                $file = $image;

                // Создание экземпляра Imagick изображения
                $image = new \Imagick($file->getRealPath());

                // Установка формата изображения в WebP
                $image->setImageFormat('webp');

                // Установка качества сжатия
                $image->setImageCompressionQuality(70);

                $fileName = 'catalog/' . uniqid() . '.webp';
                $image->writeImage(public_path('storage/' . $fileName));
                $images[] = "/storage/".$fileName;
            }
        }
        $this->merge([
            'images' => $images
        ]);
    }
}
