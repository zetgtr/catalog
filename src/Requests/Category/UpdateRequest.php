<?php

namespace Catalog\Requests\Category;

use Catalog\Models\Category;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function all($keys = null)
    {
        $data = parent::all($keys);

        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [

            'url' => ['required', Rule::unique(Category::class)->ignore($this->id)],
            ...config('catalog.request.category.update')
        ];
    }

    public function prepareForValidation()
    {
        if($this->input('parent') == "on")
        {
            $this->merge([
                'parent' => null
            ]);
        }
        if (!$this->input('url')) {
            $this->merge([
                'url' => str_slug_new($this->input('title'))
            ]);
        }
        if (!$this->input('seo_title')) {
            $this->merge([
                'seo_title' => $this->input('title')
            ]);
        }

        if ($this->file('img')) {
            $images = [];

            $category = Category::find($this->input('id'));
            $imgArr = $category->images;
            foreach ($this->file('img') as $image)
            {
                if ($imgArr && $imgArr !== '[]') {
                    foreach ($imgArr as $img) {
                        if ($img) {
                            $filePath = public_path($img);
                            if (file_exists($filePath)) {
                                unlink($filePath);
                            }
                        }
                    }
                }

                $file = $image;
                $image = new \Imagick($file->getRealPath());
                $image->setImageFormat('webp');
                $image->setImageCompressionQuality(70);
                $fileName = "/".uniqid() . '.webp';
                $folderName = 'catalog/category';
                $disk = Storage::disk('public');
                if (!$disk->exists($folderName)) {
                    $disk->makeDirectory($folderName);
                }
                $image->writeImage( $disk->path($folderName) . $fileName);
                $images[] = "/storage/".$folderName.$fileName;
            }
            $this->merge([
                'images' => $images
            ]);
        }

    }
}
