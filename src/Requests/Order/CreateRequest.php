<?php

namespace Catalog\Requests\Order;

use Catalog\Models\Product;
use Catalog\Models\Promocode;
use Catalog\QueryBuilder\CartBuilder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
      /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return config('catalog.request.order.create');
    }


    public function prepareForValidation()
    {
        $builder = new CartBuilder(request());
        if($this->phone)
        {
            $this->merge([
                'phone' => preg_replace('/\D/', '', $this->phone)
            ]);
        }

        $this->merge([
            'count' => $builder->getCount()
        ]);
        $promocode = Promocode::where('promocode',$this->promocode)->first();
        $this->merge([
            'price' => $builder->getTotalPrice($promocode)
        ]);
        $this->merge([
            'user_id' => \Auth::user()?->id
        ]);


        if($promocode)
            $this->merge([
                'promocode' => $promocode->promocode,
                'promocode_sale' => $promocode->sale,
                'promocode_type' => $promocode->type,
            ]);

        if(!$this->bonus_minus){
            $this->merge([
                'bonus_minus' => 0
            ]);
        }


        if ((int)$this->typeDelivery === 3){
             $this->merge([
                'price' => $this->sdecPrice
            ]);
        };
    }
}
