<?php

namespace Catalog\Requests\Order;

use Catalog\Models\Category;
use Catalog\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class ClickRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return config('catalog.request.order.click');
    }


    public function prepareForValidation()
    {
        if($this->phone)
        {
            $this->merge([
                'phone' => preg_replace('/\D/', '', $this->phone)
            ]);
        }
        if(!$this->count)
        {
            $this->merge([
                'count' => 1
            ]);
        }

        if(!$this->price)
        {
            $this->merge([
                'price' => 0
            ]);
        }
    }

}
