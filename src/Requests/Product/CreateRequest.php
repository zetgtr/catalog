<?php

namespace Catalog\Requests\Product;

use Catalog\Models\Category;
use Catalog\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function all($keys = null)
    {

        $data = parent::all($keys);

        if(!empty($data['images'])) {
            $images = $data['images'];

            $jsonImages = json_encode($images);

            $data['images'] = $jsonImages;
        }

        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'url' => ['required', Rule::unique(Product::class)->ignore($this->id)],
            ...config('catalog.request.product.create')
        ];
    }

    public function getCategoriesIds(): array
    {
        return (array) $this->validated('category_id');
    }

    public function prepareForValidation()
    {
        $this->merge([
            'new' => (boolean) $this->new
        ]);
        if (!$this->input('url')) {
            $this->merge([
                'url' => str_slug_new($this->title)
            ]);
        }
        if (!$this->input('seo_title')) {
            $this->merge([
                'seo_title' => $this->title
            ]);
        }

        if ($this->input('price')) {
            $this->merge([
                'filters' => $this->input('filters') ? array_merge($this->input('filters'),['price'=>$this->input('price')]) :
                    ['price'=>$this->input('price')]
            ]);
        }

        $this->merge([
            'new' => (bool) $this->new,
            'hit' => (bool) $this->hit,
        ]);


        $images = null;
        if ($this->file('img')) {
            $images = [];
            foreach ($this->file('img') as $image)
            {
                $file = $image;

                // Создание экземпляра Imagick изображения
                $image = new \Imagick($file->getRealPath());

                // Установка формата изображения в WebP
                $image->setImageFormat('webp');

                // Установка качества сжатия
                $image->setImageCompressionQuality(70);

                $fileName = "/".uniqid() . '.webp';
                $folderName = 'catalog/products';
                $disk = Storage::disk('public');
                if (!$disk->exists($folderName)) {
                    $disk->makeDirectory($folderName);
                }

                $image->writeImage( $disk->path($folderName) . $fileName);
                $images[] = "/storage/".$folderName.$fileName;
            }
        }

        $this->merge([
            'images' => $images
        ]);

    }
}
