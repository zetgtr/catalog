<?php

namespace Catalog\Requests\Product;

use Catalog\Models\Category;
use Catalog\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function all($keys = null)
    {
        $data = parent::all($keys);

        if(!empty($data['images'])) {
            $images = $data['images'];

            $jsonImages = json_encode($images);

            $data['images'] = $jsonImages;
        }

        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'url' => ['required', Rule::unique(Product::class)->ignore($this->id)],
            ...config('catalog.request.product.update')
        ];
    }

    public function getCategoriesIds(): array
    {
        return (array) $this->validated('category_id');
    }

    public function prepareForValidation()
    {
        $this->merge([
            'new' => (boolean) $this->new
        ]);
        if (!$this->input('url')) {
            $this->merge([
                'url' => str_slug_new($this->title)
            ]);
        }
        if (!$this->input('seo_title')) {
            $this->merge([
                'seo_title' => $this->title
            ]);
        }

        if ($this->input('price')) {
            $this->merge([
                'filters' => $this->input('filters') ? array_merge($this->input('filters'),['price'=>$this->input('price')]) :
                    ['price'=>$this->input('price')]
            ]);
        }


        if ($this->file('img')) {
            $images = [];
            $product = Product::find($this->input('id'));
            $imgArr = json_decode($product->images);
            $currentImagePaths = array_map(function($img) {
                return public_path($img);
            }, $imgArr);
            foreach ($this->file('img') as $image)
            {

                $file = $image;
                $filePath = $file->getRealPath();
                $folderName = 'catalog/products';
                $fileName = $file->getClientOriginalName();
                $filePathSecond = public_path('storage/'.$folderName.'/'.$fileName);
                if(!in_array($filePathSecond,$currentImagePaths)){
                    $imageObj = new \Imagick($filePath);
                    $imageObj->setImageFormat('webp');

                    $fileName = "/" . uniqid() . '.webp';
                    $imageObj->stripImage();
                    $imageObj->setImageCompressionQuality(75);
                    $disk = Storage::disk('public');

                    if (!$disk->exists($folderName)) {
                        $disk->makeDirectory($folderName);
                    }

                    $newFilePath = $disk->path($folderName) . $fileName;

                    $imageObj->writeImage($newFilePath);

                    $newImagePaths[] = "/storage/" . $folderName . $fileName;
                }else{

                    $newImagePaths[] = '/storage/'.$folderName.'/'.$fileName;
                }
            }
            foreach ($currentImagePaths as $path) {
                if (!in_array($path, array_map(function($newPath) {
                    return public_path($newPath);
                }, $newImagePaths))) {
                    if (file_exists($path)) {
                        unlink($path);
                    }
                }
            }
            $this->merge([
                'images' => array_reverse($newImagePaths)
            ]);
        }


    }
}
