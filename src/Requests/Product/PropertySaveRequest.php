<?php

namespace Catalog\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class PropertySaveRequest extends FormRequest
{
    public function rules()
    {
        return [
            ...config('catalog.request.product.property')
        ];
    }

    public function authorize()
    {
        return true;
    }
}
