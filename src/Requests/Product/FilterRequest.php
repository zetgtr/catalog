<?php

namespace Catalog\Requests\Product;

use Catalog\Models\Category;
use Catalog\Models\CategoryFilter;
use Catalog\QueryBuilder\FilterBuilder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\View;

class FilterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return config('catalog.request.product.filter');
    }


    public function prepareForValidation()
    {
        $builder = new FilterBuilder();
        $segments = explode('/', $this->route('filters'));
        if($this->checkbox || count($segments))
            $this->merge([
                'checkbox' => $this->checkbox ? array_merge($this->checkbox, $builder->setCheckbox($segments)) : $builder->setCheckbox($segments),
            ]);
        else
            $this->merge([
                'checkbox' => []
            ]);
        $catetegory = Category::where('url',$this->url ?? $this->category )->first();
        if($catetegory)
        $this->merge([
                'category_id' => $catetegory->id
            ]);

        View::share('filtersRequest',$this->all());

    }
}
