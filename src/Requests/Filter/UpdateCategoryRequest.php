<?php

namespace Catalog\Requests\Filter;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return config('catalog.request.filter.updateCategory');
    }
    public function prepareForValidation()
    {
        $this->merge([
            'open' => $this->open ? $this->open : '',
            'hidden' => (boolean) $this->hidden
        ]);
    }
}
