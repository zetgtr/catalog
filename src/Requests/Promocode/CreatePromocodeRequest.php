<?php

namespace Catalog\Requests\Promocode;

use Illuminate\Foundation\Http\FormRequest;

class CreatePromocodeRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'promocode' => ['required','string','unique:catalog_promocodes,promocode'],
            'type' => ['required','string'],
            'sale' => ['required','integer','min:1',function ($attribute, $value, $fail) {
                $type = $this->input('type');
                if ($type === 'procent' && $value > 100) {
                    $fail('Максимальное значение для скидки в процентах должно быть 100.');
                }
            }]
        ];
    }

    public function attributes(): array
    {
        return [
            'promocode' => 'промокод',
            'type' => 'тип скидки',
            'sale' => 'скидка',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
